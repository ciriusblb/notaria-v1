'use strict'

var connection = require('../../../config/mysql/connection')

var dataModels = {
	getNosotros : function(callback){
		if (connection) {
			var sql = `select * from h_nosotros`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},
	getValores: function(callback){
		if(connection){
			var sql = `select * from h_valores`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},
	A_getNosotros:function(callback){
		if(connection){
			var sql = `select * from h_nosotros`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},
	A_saveNosotro:function(data,callback){
		if(connection){
			var sql = `insert into h_nosotros(titulo,descripcion) values(`+
			connection.escape(data.titulo)+`,`+
			connection.escape(data.descripcion)+`)`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,{id:row.insertId});
				}
			});
		}
	},
	A_editNosotro:function(data,callback){
		if(connection){
			var sql = 'update h_nosotros set titulo = '+
			connection.escape(data.titulo)+', descripcion = '+
			connection.escape(data.descripcion)+' where id = '+connection.escape(data.id);
				connection.query(sql, function(error,row){
					if (error) throw error
					else{
						callback(null,{msg:'editado correctamente'});
					}
				});
		}
	},
	A_removeNosotro:function(data,callback){
		if(connection){
			var sql = 'delete from h_nosotros where id = '+connection.escape(data.id);
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,{msg:'eliminado correctamente'});
				}
			});
		}
	},
	A_getvalores:function(callback){
		if(connection){
			var sql = `select * from h_valores`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},
	A_savevalor:function(data,callback){
		if(connection){
			var sql = `insert into h_valores(titulo,descripcion) values(`+
			connection.escape(data.titulo)+`,`+
			connection.escape(data.descripcion)+`)`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,{id:row.insertId});
				}
			});
		}
	},
	A_editvalor:function(data,callback){
		if(connection){
			var sql = 'update h_valores set titulo = '+
			connection.escape(data.titulo)+', descripcion = '+
			connection.escape(data.descripcion)+' where id = '+connection.escape(data.id);
				connection.query(sql, function(error,row){
					if (error) throw error
					else{
						callback(null,{msg:'editado correctamente'});
					}
				});
		}
	},
	A_removevalor:function(data,callback){
		if(connection){
			var sql = 'delete from h_valores where id = '+connection.escape(data.id);
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,{msg:'eliminado correctamente'});
				}
			});
		}
	}
};

module.exports = dataModels

