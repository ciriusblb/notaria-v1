'use strict'

var connection = require('../../../config/mysql/connection')

var dataModels = {
	getEnlaces : function(callback){
		if (connection) {
			var sql = `select * from h_enlace`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},	
	getComunicado : function(callback){
		console.log("entro x2");
		if (connection) {
			var sql = `select * from h_informe order by id desc limit 1`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row[0]);
				}
			});
		}
	}
};

module.exports = dataModels

