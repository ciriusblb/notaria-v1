'use strict'

var connection = require('../../../config/mysql/connection')

var dataModels = {
	getPreguntas : function(callback){
		if (connection) {
			var sql = `select * from h_preguntas`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},
	A_getPreguntas:function(callback){
		if(connection){
			var sql = `select * from h_preguntas`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},
	A_savePregunta:function(data,callback){
		if(connection){
			var sql = `insert into h_preguntas(pregunta,respuesta) values(`+
			connection.escape(data.pregunta)+`,`+
			connection.escape(data.respuesta)+`)`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,{id:row.insertId});
				}
			});
		}
	},
	A_editPregunta:function(data,callback){
		if(connection){
			var sql = 'update h_preguntas set pregunta = '+
			connection.escape(data.pregunta)+', respuesta = '+
			connection.escape(data.respuesta)+' where id = '+connection.escape(data.id);
				connection.query(sql, function(error,row){
					if (error) throw error
					else{
						callback(null,{msg:'editado correctamente'});
					}
				});
		}
	},
	A_removePregunta:function(data,callback){
		if(connection){
			var sql = 'delete from h_preguntas where id = '+connection.escape(data.id);
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,{msg:'eliminado correctamente'});
				}
			});
		}
	}
};

module.exports = dataModels

