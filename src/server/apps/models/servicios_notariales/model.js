'use strict'

var connection = require('../../../config/mysql/connection')

var dataModels = {

	// Escrituras publicas
	getcompra_venta:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Escrituras Públicas' and subvista='Compra Venta'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}			
	},
	getconstitucion_de_hipotecas:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Escrituras Públicas' and subvista='Constitucion de Hipotecas'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
					        console.log(row);
							callback(null,row);
					    })
					}

				}
			});
		}	
	},
	getconstitucion_de_sociedades:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Escrituras Públicas' and subvista='Constitucion de Sociedades'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},
	getestructura_poderes:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Escrituras Públicas' and subvista='Poderes'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},

	//transferncias vehiculares
	gettransferencias_vehiculares:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Transferencias Vehiculares'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},

	//asuntos no contenciosos
	getdivorcios:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Asuntos no Contenciosos' and subvista='Divorcios'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},
	getpatrimonio_familiar:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Asuntos no Contenciosos' and subvista='Patrimonio Familiar'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},
	getrectificacion_de_partidas:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Asuntos no Contenciosos' and subvista='Rectificación de Partidas'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},
	getsucesion_interesada:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Asuntos no Contenciosos' and subvista='Sucesión Interesada'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},

	//protestos 
	getprotestos:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Protestos'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},

	//cartas notariales
	getcartas_notariales:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Cartas Notariales'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},

	// poderes
	getpoderes:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Poderes'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},

	//certificaciones 
	getcertificaciones:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Certificaciones'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}
	},

	//legalizaciones
	getde_firmas:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Legalizaciones' and subvista='De firmas'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}	
	},
	getde_copias:function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales where vista='Legalizaciones' and subvista='De copias'`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					if(!row[0]){
						callback(null,[]);
					}else{
						var requisitos =[];
						getRequisitos(row[0].id).then(data=>{
					        requisitos=data;
					        row[0].requisitos=requisitos;
							callback(null,row);
					    })
					}
				}
			});
		}	
	},


	A_getServiciosNotariales : function(callback){
		if (connection) {
			var sql = `select * from h_servicios_notariales`;
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					callback(null,row);
				}
			});
		}
	},
	A_getServicioNotarial : function(data,callback){
		if (connection) {
			var sql = `select * from h_requisito where id_vista=`+connection.escape(data.id);
			connection.query(sql,function(error,row){
				if(error) throw error;
				else{
					var requisitos= row;
					var sql = `select * from h_servicios_notariales where id=`+connection.escape(data.id);
					connection.query(sql, function(error,row){
						if (error) throw error
						else{
							row[0].requisitos=requisitos;
							console.log('row[0] ',row[0]);
							callback(null,row[0]);
						}
					});
				}
			})
		}
	},

	A_saveServicioNotarial : function(data,callback){
		if(connection){
			var sql = 'insert into h_servicios_notariales(vista,subvista,lugar,nota) values('+
			connection.escape(data.vista)+','+
			connection.escape(data.subvista)+','+
			connection.escape(data.lugar)+','+
			connection.escape(data.nota)+')';
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					var id = row.insertId;
					var sql='';
					for (var i = 0; i < data.cantidadRequisitos; i++) {
					    sql = sql+'('+connection.escape(data.requisitos[i].requisito)+','+connection.escape(id)+')';
						if(i<data.cantidadRequisitos-1){
							sql=sql+',';
						}
					}
					sql = 'insert into h_requisito(requisito,id_vista) values'+sql;
					connection.query(sql,function(error,row){
						if(error) throw error;
						else{
							callback(null, {id:id});
						}
					})
				}
			});
		}
	},
	A_editServicioNotarial : function(data,callback){
		if(connection){
			var sql = 'update h_servicios_notariales set vista ='+
			connection.escape(data.vista)+', subvista = '+
			connection.escape(data.subvista)+', lugar = '+
			connection.escape(data.lugar)+', nota = '+
			connection.escape(data.nota)+' where  id = '+connection.escape(data.id);
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					for (var i = 0; i < data.cantidadRequisitos; i++) {
						var sql = 'call spEditServicioNotarial('+connection.escape(data.requisitos[i].requisito)+','+
						connection.escape(data.requisitos[i].id)+','+
						connection.escape(data.id)+','+
						connection.escape(data.requisitos[i].agregar)+')';
						connection.query(sql,function(error,row){
							if(error) throw error;
						})
					}
					if(data.eliminarId.length>0){
						for (var i = 0; i < data.eliminarId.length; i++) {
							var sql = 'delete from h_requisito where id='+connection.escape(data.eliminarId[i]);
							connection.query(sql,function(error,row){
								if(error) throw error;
							})
						}	
					}
					callback(null, {mdg:'editado correctamente'});
				}
			});
		}
	},
	A_removeServicioNotarial : function(data,callback){
		if(connection){
			console.log('data ',data);
			var sql = 'delete from h_servicios_notariales where id = '+connection.escape(data.id);
			connection.query(sql, function(error,row){
				if (error) throw error
				else{
					var eliminados=data.eliminarId.split(',');
					for (var i = 0; i < eliminados.length; i++) {
						var sql = 'delete from h_requisito where id='+connection.escape(eliminados[i]);
						connection.query(sql,function(error,row){
							if(error) throw error;
						})
					}
					callback(null,{msg:'eliminados correctamente'});
				}
			});
		}
	}
};

function getRequisitos(id){
	return new Promise((resolve,reject)=>{
		var sql = `select * from h_requisito where id_vista = ${id}`;
		connection.query(sql,function(error,row){
			if(error) reject(error);
			else{
				resolve(row);
			}
		})
	})

}
module.exports = dataModels

