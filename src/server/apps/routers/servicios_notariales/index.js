'use strict'

const express = require('express')
const controller = require('../../controllers/servicios_notariales/controller')
const isAuth = require('../../middlewares/login')

const servicios_notariales = express.Router();

// estructuras publicas
servicios_notariales.route('/compra_venta')
	.get(controller.getcompra_venta)
servicios_notariales.route('/constitucion_de_hipotecas')
	.get(controller.getconstitucion_de_hipotecas)
servicios_notariales.route('/constitucion_de_sociedades')
	.get(controller.getconstitucion_de_sociedades)
servicios_notariales.route('/estructura_poderes')
	.get(controller.getestructura_poderes)


// transferencias vehiculares

servicios_notariales.route('/transferencias_vehiculares')
	.get(controller.gettransferencias_vehiculares)


//asuntos no contenciosos
servicios_notariales.route('/divorcios')
	.get(controller.getdivorcios)

servicios_notariales.route('/patrimonio_familiar')
	.get(controller.getpatrimonio_familiar)

servicios_notariales.route('/rectificacion_de_partidas')
	.get(controller.getrectificacion_de_partidas)

servicios_notariales.route('/sucesion_interesada')
	.get(controller.getsucesion_interesada)

//protestos
servicios_notariales.route('/protestos')
	.get(controller.getprotestos)

//Cartas Notariales
servicios_notariales.route('/cartas_notariales')
	.get(controller.getcartas_notariales)



//Poderes
servicios_notariales.route('/poderes')
	.get(controller.getpoderes)

//certificaciones
servicios_notariales.route('/certificaciones')
	.get(controller.getcertificaciones)


//legalizaciones
servicios_notariales.route('/de_firmas')
	.get(controller.getde_firmas)
servicios_notariales.route('/de_copias')
	.get(controller.getde_copias)


servicios_notariales.route('/A_servicios_notariales')
	.get(isAuth,controller.A_getServiciosNotariales)
	.post(controller.A_saveServicioNotarial);

servicios_notariales.route('/A_servicios_notariales/:id')
	.get(controller.A_getServicioNotarial)
	.put(controller.A_editServicioNotarial)
	.delete(controller.A_removeServicioNotarial);



module.exports = servicios_notariales;