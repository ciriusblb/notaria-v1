'use strict'

const express = require('express')
const controller = require('../../controllers/preguntas_frecuentes/controller')
const preguntas_frecuentes = express.Router();

const isAuth = require('../../middlewares/login')

preguntas_frecuentes.route('/Preguntas')
	.get(controller.getPreguntas);

preguntas_frecuentes.route('/A_Preguntas')
	.get(isAuth,controller.A_getPreguntas)
	.post(controller.A_savePregunta);

preguntas_frecuentes.route('/A_Preguntas/:id')
	.put(controller.A_editPregunta)
	.delete(controller.A_removePregunta);

module.exports = preguntas_frecuentes