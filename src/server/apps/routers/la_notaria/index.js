'use strict'

const express = require('express')
const controller = require('../../controllers/la_notaria/controller')
const la_notaria = express.Router();

const isAuth = require('../../middlewares/login')

la_notaria.route('/Nosotros')
	.get(controller.getNosotros);

la_notaria.route('/A_Nosotros')
	.get(isAuth,controller.A_getNosotros)
	.post(controller.A_saveNosotro);

la_notaria.route('/A_Nosotros/:id')
	.put(controller.A_editNosotro)
	.delete(controller.A_removeNosotro);


la_notaria.route('/Valores')
	.get(controller.getValores);

la_notaria.route('/A_Valores')
	.get(isAuth,controller.A_getvalores)
	.post(controller.A_savevalor);

la_notaria.route('/A_Valores/:id')
	.put(controller.A_editvalor)
	.delete(controller.A_removevalor);


module.exports = la_notaria