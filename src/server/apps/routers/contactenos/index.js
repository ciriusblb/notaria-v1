'use strict'

const express = require('express')
const  sendEmail = require('./emailModel');
const contacto = express.Router();

contacto.route('/contacto')
 	.post(function(req,res){
 		sendEmail(req.body,function(error,data){
 			res.send(data);
 		})
 	});

module.exports = contacto;