'use strict';
var nodemailer = require('nodemailer');
var sendEmail = function(data, callback){

  var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: data.email,
        pass: data.password,
    }
  });
  var mailOptions = {
    from: data.email,
    to: 'ciriusblb@gmail.com',
    subject: 'Asunto',
    text:data.mensaje
  };
  transporter.sendMail(mailOptions, function(error, response){

      if (error){
        callback(null,undefined);
    } else {
      console.log('response-2 ',response);

        callback(null,mailOptions);
    }
  });




};

module.exports=sendEmail;
