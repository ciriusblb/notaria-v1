'use strict'

const dataModels = require('../../models/la_notaria/model')
const upload = require('../../services/upload');


var controllers ={
	getNosotros: function(req,res){
		dataModels.getNosotros((error,data)=>{
		    if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	getValores: function(req,res){
		dataModels.getValores((error,data)=>{
		    if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_getNosotros : function(req,res){
		dataModels.A_getNosotros((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_saveNosotro : function(req,res){
		dataModels.A_saveNosotro(req.body,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_editNosotro : function(req,res){
		dataModels.A_editNosotro(req.body,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_removeNosotro : function(req,res){
		dataModels.A_removeNosotro(req.params,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_getvalores : function(req,res){
		dataModels.A_getvalores((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_savevalor : function(req,res){
		dataModels.A_savevalor(req.body,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_editvalor : function(req,res){
		dataModels.A_editvalor(req.body,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_removevalor : function(req,res){
		dataModels.A_removevalor(req.params,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	}
}






module.exports =controllers;