'use strict'

const dataModels = require('../../models/servicios_notariales/model');


var controllers = {
	// estructuras publicas
	getcompra_venta: function(req,res){
		dataModels.getcompra_venta((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})	
			res.status(200).send(data)
		})
	},
	getconstitucion_de_hipotecas: function(req,res){
		dataModels.getconstitucion_de_hipotecas((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	getconstitucion_de_sociedades: function(req,res){
		dataModels.getconstitucion_de_sociedades((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	getestructura_poderes: function(req,res){
		dataModels.getestructura_poderes((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},

	// transferencias vehiculares
	gettransferencias_vehiculares: function(req,res){
		dataModels.gettransferencias_vehiculares((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},


	// asuntos no contenciosos
	getdivorcios:function(req,res){
		dataModels.getdivorcios((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	getpatrimonio_familiar:function(req,res){
		dataModels.getpatrimonio_familiar((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	getrectificacion_de_partidas:function(req,res){
		dataModels.getrectificacion_de_partidas((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	getsucesion_interesada:function(req,res){
		dataModels.getsucesion_interesada((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},

	// protestos
	getprotestos:function(req,res){
		dataModels.getprotestos((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},

	// cartas notariales
	getcartas_notariales:function(req,res){
		dataModels.getcartas_notariales((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},

	//poderes 
	getpoderes:function(req,res){
		dataModels.getpoderes((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},

	//certificaciones
	getcertificaciones:function(req,res){
		dataModels.getcertificaciones((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},

	//legalizaciones
	getde_firmas:function(req,res){
		dataModels.getde_firmas((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	getde_copias:function(req,res){
		dataModels.getde_copias((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},

	A_getServiciosNotariales : function(req,res){
		dataModels.A_getServiciosNotariales((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_getServicioNotarial : function(req,res){
		dataModels.A_getServicioNotarial(req.params,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${error}`})			
			res.status(200).send(data)
		})
	},
	A_saveServicioNotarial : function(req,res){
		dataModels.A_saveServicioNotarial(req.body,(error,data)=>{
			res.status(200).send(data);
		})
	},
	A_editServicioNotarial : function(req,res){
		dataModels.A_editServicioNotarial(req.body,(error,data)=>{
			res.send(data);
		})
	},
	A_removeServicioNotarial : function(req,res){
		var eliminados = {
			id:req.params.id,
			eliminarId:req.query.eliminarId.toString()
		}

		dataModels.A_removeServicioNotarial(eliminados,(error,data)=>{
			res.send(data);
		})
	}
}

module.exports = controllers;