'use strict'

const dataModels = require('../../models/preguntas_frecuentes/model')


var controllers ={
	getPreguntas: function(req,res){
		dataModels.getPreguntas((error,data)=>{
		    if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_getPreguntas : function(req,res){
		dataModels.A_getPreguntas((error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_savePregunta : function(req,res){
		dataModels.A_savePregunta(req.body,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_editPregunta : function(req,res){
		dataModels.A_editPregunta(req.body,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	},
	A_removePregunta : function(req,res){
		dataModels.A_removePregunta(req.params,(error,data)=>{
			if (error) return res.status(500).send({message: `se ha producido un error ${error}`})
			if (!data) return res.status(404).send({message: `no se ha podido encontrar el elemento ${err}`})			
			res.status(200).send(data)
		})
	}
}






module.exports =controllers;