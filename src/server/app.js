const express = require("express");
const app = express();
const morgan = require("morgan");
const formidable= require('express-form-data');



//settings
app.set('port', process.env.PORT || 4000);
app.use(morgan('dev'));
app.use(express({limit: '100mb'}));
app.use(express.urlencoded({extended:true}));
app.use(formidable.parse({keepExtensions:true}));
app.use(express.json());



var getHome = require('./apps/routers/home');
var getSidebar = require('./apps/routers/sidebar');
var getServiciosNotariales = require('./apps/routers/servicios_notariales');
var getLaNotaria = require('./apps/routers/la_notaria');
var getPreguntasFrecuentes = require('./apps/routers/preguntas_frecuentes');

var contactenos = require('./apps/routers/contactenos');


var login = require('./apps/routers/login/login');
app.use('/',getHome);
app.use('/',getSidebar);
app.use('/',getServiciosNotariales);
app.use('/',getLaNotaria);
app.use('/',getPreguntasFrecuentes);
app.use('/',contactenos);
app.use('/',login);





// app.use('/', express.static('../'));
app.use('/', express.static('./src/server/public'));
app.use('/', express.static('./src/client'));
app.use('/', express.static('./'));

//routes
app.get('*',(req,res)=>{
	res.sendfile('./src/client/index.html');
});

//starting the server

app.listen(app.get('port'),()=>{
	console.log(`server listening in PORT ${app.get('port')}`);
});