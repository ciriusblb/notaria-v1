(function (){
	'use strict'
	angular.module('app.client.home')
	.factory('contactoService',dataService);

	function dataService ($resource){
		var resource = $resource('/contacto/:id',{id:'@id'},{
			'send' : {method: 'POST'}
		})

		var service = {
			enviarMensaje:enviarMensaje
		}

		return service;

		function enviarMensaje(data){
		return resource.send(data).$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
