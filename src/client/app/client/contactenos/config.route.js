(function(){
	'use strict';
	angular.module('app.client.contactenos')
		.run(appRun);

	appRun.$inject=['routehelper'];

	function appRun(routehelper){
		return routehelper.configureRoutes(getRoutes());
	}

	function getRoutes(){
		return [
			{
				name:'Client.contactenos',
				config:{
					url:'/Contactenos',
					templateUrl:'app/client/contactenos/contactenos.html',
					controller:'contactenos',
					controllerAs:'vm',
					title:'Contactenos',
					settings:{
						nav:5,
						content:'Contáctenos'
					}
				}
			}
		]
	}

}());