(function(){
	angular.module('app.client.preguntas')
		.controller('preguntas',preguntas);

	preguntas.$inject=['preguntasService','servicios'];
	function preguntas(preguntasService,servicios){
		var vm = this;
		vm.preguntas=[];
		init();
		function init(){
			preguntasService.getPreguntas().then(function(data){
				vm.preguntas=data;
				for (var i = 0; i < vm.preguntas.length; i++) {
                    vm.preguntas[i].texto=servicios.textToArray(vm.preguntas[i].respuesta);
                }
                console.log(vm.preguntas);
			})
		}
	}
}())
