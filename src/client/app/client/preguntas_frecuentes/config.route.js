(function(){
	'use strict';
	angular.module('app.client.preguntas')
		.run(appRun);

	appRun.$inject=['routehelper'];

	function appRun(routehelper){
		return routehelper.configureRoutes(getRoutes());
	}

	function getRoutes(){
		return [
			{
				name:'Client.preguntas',
				config:{
					url:'/preguntas',
					templateUrl:'app/client/preguntas_frecuentes/preguntas_frecuentes.html',
					controller:'preguntas',
					controllerAs:'vm',
					title:'Preguntas Frecuentes',
					settings:{
						nav:4,
						content:'Preguntas Frecuentes'
					}
				}
			}
		]
	}

}());