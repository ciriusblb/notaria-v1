(function (){
	'use strict'
	angular.module('app.client.preguntas')
	.factory('preguntasService',dataService);

	function dataService ($resource){
		var resource = $resource('/Preguntas/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getPreguntas:getPreguntas
		}

		return service;

		function getPreguntas(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
