(function(){
	'use strict';
	angular.module('app.client',[
		'app.client.home',
		'app.client.servicios_notariales',
		'app.client.la_notaria',
		'app.client.preguntas',
		// 'app.client.servicios',
		// 'app.client.eventos',
		'app.client.contactenos',
		'app.client.noticias',
		'app.client.noticia'

		]);
}());