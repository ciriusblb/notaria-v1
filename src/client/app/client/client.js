(function(){
	angular.module('app.client')
		.controller('Client',Client);

	Client.$inject=['$state','routehelper','$scope','clientService'];
	function Client($state,routehelper,$scope,clientService){


		var vm = this;
		vm.fecha = new Date();

		$scope.slickConfig3 = {
            dots: true,
            infinite: true,
            slidesToShow:6,
            slidesToScroll: 1,
            arrows: true,
 			prevArrow: '<div class="class-to-style-left"><span class="fa fa-angle-left"></span><span class="sr-only">Prev</span></div>',
			nextArrow: '<div class="class-to-style-right"><span class="fa fa-angle-right"></span><span class="sr-only">Next</span></div>',
            responsive: [
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };
		var routes = routehelper.getSubRoutes('Client');
		vm.navRoutes=routes;
		init();
		function init(){
			activate();
			getComunicado();
		}
		function getNavRoutes(){
			vm.navRoutes = routes.filter(function(r){
				return r.settings && r.settings.nav;
			}).sort(function(r1,r2){
				return r1.settings.nav - r2.settings.nav;
			});
		}
		function isCurrent(route){
			if(!route.title || !$state.current || !$state.current.title){
				return '';
			}
			var menuName = route.title;	
			return $state.current.title.substr(0,menuName.length) === menuName ? 'current':'';
		}
		function activate(){
		    clientService.getEnlaces().then(function(data){
                vm.enlaces=data;
                if(vm.enlaces){
                	$scope.dataLoaded = true;
                }
            })
		}

		// init();		
		// vm.informe={
		// 	id:1,
		// 	header:'Ceremonia 1',
		// 	titulo:'titulo 1',
		// 	descripcion:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam',
		// 	informe:'informe0001.pdf',
		// 	nombre_archivo:'nombre del informe 0001',
		// 	fecha:new Date(),
		// 	fecha_registro:new Date()
		// }
		function getComunicado(){
	      clientService.getComunicado().then(function(data){
	        vm.informe=data;
	      })
		}
	}
}())
