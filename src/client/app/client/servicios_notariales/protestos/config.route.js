(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.protestos')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.protestos',
		 		config : {
		 			url:'/Protestos',
		 			templateUrl : 'app/client/servicios_notariales/protestos/protestos.html',	 			 
		 			title : 'Protestos',
		 			controller:'protestos',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :4,
			 				content : 'Protestos'
		 			}
		 		}
		 	}
	 	];
	 }
}());