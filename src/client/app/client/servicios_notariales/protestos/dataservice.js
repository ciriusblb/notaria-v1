(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.protestos')
	.factory('protestosService',dataService);

	function dataService ($resource){
		var resource = $resource('/protestos/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getprotestos:getprotestos
		}

		return service;

		function getprotestos(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
