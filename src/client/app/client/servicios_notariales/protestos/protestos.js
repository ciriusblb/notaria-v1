(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.protestos')
		.controller('protestos',protestos);
    protestos.$inject = ['protestosService','servicios'];
	function protestos(protestosService,servicios){
		var vm = this;
		init();
		function init(){
			protestosService.getprotestos().then(function(data){
				vm.protestos=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;

                console.log('vm.protestos ',data);
			})
		}
	}	
}());