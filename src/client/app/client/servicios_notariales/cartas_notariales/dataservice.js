(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.cartas_notariales')
	.factory('cartas_notarialesService',dataService);

	function dataService ($resource){
		var resource = $resource('/cartas_notariales/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getcartas_notariales:getcartas_notariales
		}

		return service;

		function getcartas_notariales(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
