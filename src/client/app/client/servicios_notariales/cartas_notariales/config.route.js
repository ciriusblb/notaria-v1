(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.cartas_notariales')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.cartas_notariales',
		 		config : {
		 			url:'/Cartas_Notariales',
		 			templateUrl : 'app/client/servicios_notariales/cartas_notariales/cartas_notariales.html',		 			 
		 			title : 'Cartas_Notariales',
		 			controller:'cartas_notariales',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :5,
			 				content : 'Cartas Notariales'
		 			}
		 		}
		 	}
	 	];
	 }
}());