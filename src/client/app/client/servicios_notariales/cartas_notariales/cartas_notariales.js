(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.cartas_notariales')
		.controller('cartas_notariales',cartas_notariales);
    cartas_notariales.$inject = ['cartas_notarialesService','servicios'];
	function cartas_notariales(cartas_notarialesService,servicios){
		var vm = this;
		init();
		function init(){
			cartas_notarialesService.getcartas_notariales().then(function(data){
				vm.cartas_notariales=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar = data[0].lugar;
				vm.nota = data[0].nota;
                console.log('vm.cartas_notariales ',data);
			})
		}
	}	
}());