(function(){
	angular.module('app.client.servicios_notariales')
		.controller('servicios_notariales',servicios_notariales);

	servicios_notariales.$inject=['$state','routehelper','$scope'];
	function servicios_notariales($state,routehelper,$scope){
		var vm = this;
		var routes = routehelper.getSubRoutes('Client');
		vm.cardRoutes = routes[1].children;
		vm.routeActivated="";
		vm.subRouteActivated="";
		$scope.$watch(function(){
		    return $state.$current.name;
		}, function(newVal, oldVal){
			if(newVal.split('.').length>3){
				vm.routeActivated=newVal.slice(0, -(newVal.split('.')[3].length+1));
				vm.subRouteActivated=newVal;
			}else{
				vm.routeActivated=newVal;
			}
		}) 
	}
}())
