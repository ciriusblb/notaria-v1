(function(){
	'use strict';
	angular.module('app.client.servicios_notariales')
		.run(appRun);

	appRun.$inject=['routehelper'];

	function appRun(routehelper){
		return routehelper.configureRoutes(getRoutes());
	}

	function getRoutes(){
		return [
			{
				name:'Client.servicios_notariales',
				config:{
					url:'/Servicios_notariales',
					templateUrl:'app/client/servicios_notariales/servicios_notariales.html',
					children:[],
					title:'Servicios_Notariales',
					controller:'servicios_notariales',
					controllerAs:'vm',
					settings:{
						nav:2,
						content:'Servicios Notariales <i class="fas fa-angle-down"></i>'
					}
				}
			}
		]
	}

}());