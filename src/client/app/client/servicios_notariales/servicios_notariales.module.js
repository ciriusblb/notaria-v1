(function(){
	'use strict';
	angular.module('app.client.servicios_notariales',[
		'app.client.servicios_notariales.estructuras_publicas',
		'app.client.servicios_notariales.transferencias_vehiculares',
		'app.client.servicios_notariales.asuntos_no_contenciosos',
		'app.client.servicios_notariales.protestos',
		'app.client.servicios_notariales.cartas_notariales',
		'app.client.servicios_notariales.poderes',
		'app.client.servicios_notariales.legalizaciones'
		]);
}());