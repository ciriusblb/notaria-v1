(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.legalizaciones')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.legalizaciones.de_copias',
		 		config : {
		 			url:'/De_Copias',
		 			templateUrl : 'app/client/servicios_notariales/legalizaciones/de_copias/de_copias.html',		 			 
		 			title : 'De_Copias',
		 			controller:'de_copias',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :2,
			 				content : 'De copias'
		 			}
		 		}
		 	}
	 	];
	 }
}());