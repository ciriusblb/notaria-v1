(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.legalizaciones')
		.controller('de_copias',de_copias);
    de_copias.$inject = ['de_copiasService','servicios'];
	function de_copias(de_copiasService,servicios){
		var vm = this;
		init();
		function init(){
			de_copiasService.getde_copias().then(function(data){
				vm.de_copias=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.de_copias ',data);
			})
		}
	}	
}());