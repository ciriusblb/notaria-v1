(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.legalizaciones')
	.factory('de_copiasService',dataService);

	function dataService ($resource){
		var resource = $resource('/de_copias/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getde_copias:getde_copias
		}

		return service;

		function getde_copias(){
		return resource.query().$promise
			.then(function(data){
				console.log('service ',data);
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
