(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.legalizaciones')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.legalizaciones.de_firmas',
		 		config : {
		 			url:'/De_Firmas',
		 			templateUrl : 'app/client/servicios_notariales/legalizaciones/de_firmas/de_firmas.html',		 			 
		 			title : 'De_Firmas',
		 			controller:'de_firmas',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :1,
			 				content : 'De firmas'
		 			}
		 		}
		 	}
	 	];
	 }
}());