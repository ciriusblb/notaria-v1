(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.legalizaciones')
	.factory('de_firmasService',dataService);

	function dataService ($resource){
		var resource = $resource('/de_firmas/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getde_firmas:getde_firmas
		}

		return service;

		function getde_firmas(){
		return resource.query().$promise
			.then(function(data){
				console.log('service ',data);
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
