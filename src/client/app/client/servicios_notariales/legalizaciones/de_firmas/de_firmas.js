(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.legalizaciones')
		.controller('de_firmas',de_firmas);
    de_firmas.$inject = ['de_firmasService','servicios'];
	function de_firmas(de_firmasService,servicios){
		var vm = this;
		init();
		function init(){
			de_firmasService.getde_firmas().then(function(data){
				vm.de_firmas=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;

                console.log('vm.de_firmas ',data);
			})
		}
	}	
}());