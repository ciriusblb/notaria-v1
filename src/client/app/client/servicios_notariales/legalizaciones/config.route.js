(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.legalizaciones')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.legalizaciones',
		 		config : {
		 			url:'/Legalizaciones',
		 			templateUrl : 'app/client/servicios_notariales/legalizaciones/legalizaciones.html',		 			 
		 			title : 'Legalizaciones',
		 			children:[],
		 			go:'de_firmas',
		 			settings : {
			 				nav :8,
			 				content : 'Legalizaciones'
		 			}
		 		}
		 	}
	 	];
	 }
}());