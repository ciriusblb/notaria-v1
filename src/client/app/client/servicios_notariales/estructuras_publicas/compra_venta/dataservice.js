(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.estructuras_publicas')
	.factory('compra_ventaService',dataService);

	function dataService ($resource){
		var resource = $resource('/compra_venta/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getcompra_venta:getcompra_venta
		}

		return service;

		function getcompra_venta(){
		return resource.query().$promise
			.then(function(data){
				console.log('service ',data);
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
