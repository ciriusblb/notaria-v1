(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.estructuras_publicas')
		.controller('compra_venta',compra_venta);
    compra_venta.$inject = ['compra_ventaService','servicios'];
	function compra_venta(compra_ventaService,servicios){
		var vm = this;
		init();
		function init(){
			compra_ventaService.getcompra_venta().then(function(data){
				vm.compra_venta=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.compra_venta ',data);
			})
		}
	}	
}());