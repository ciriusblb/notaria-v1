(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.estructuras_publicas')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.estructuras_publicas.compra_venta',
		 		config : {
		 			url:'/Compra_Venta',
		 			templateUrl : 'app/client/servicios_notariales/estructuras_publicas/compra_venta/compra_venta.html',
		 			title : 'Compra_Venta',
		 			controller:'compra_venta',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :1,
			 				content : 'Compra Venta'
		 			}
		 		}
		 	}
	 	];
	 }
}());