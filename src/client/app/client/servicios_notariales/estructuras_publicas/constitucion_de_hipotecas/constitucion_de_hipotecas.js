(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.estructuras_publicas')
		.controller('constitucion_de_hipotecas',constitucion_de_hipotecas);
    constitucion_de_hipotecas.$inject = ['constitucion_de_hipotecasService','servicios'];
	function constitucion_de_hipotecas(constitucion_de_hipotecasService,servicios){
		var vm = this;
		init();
		console.log("constitucion_de_hipotecas ",constitucion_de_hipotecasService );
		function init(){
			constitucion_de_hipotecasService.getconstitucion_de_hipotecas().then(function(data){
				console.log(data)
				vm.constitucion_de_hipotecas=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.constitucion_de_hipotecas ',vm.constitucion_de_hipotecas);
			})
		}
	}	
}());