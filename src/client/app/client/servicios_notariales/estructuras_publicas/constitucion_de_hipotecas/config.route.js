(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.estructuras_publicas')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.estructuras_publicas.constitucion_de_hipotecas',
		 		config : {
		 			url:'/Constitucion_de_Hipotecas',
		 			templateUrl : 'app/client/servicios_notariales/estructuras_publicas/constitucion_de_hipotecas/constitucion_de_hipotecas.html',
		 			title : 'Constitucion_de_Hipotecas',
		 			controller:'constitucion_de_hipotecas',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :4,
			 				content : 'Constitución de Hipotecas'
		 			}
		 		}
		 	}
	 	];
	 }
}());