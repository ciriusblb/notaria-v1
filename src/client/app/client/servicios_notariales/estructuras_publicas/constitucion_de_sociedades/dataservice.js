(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.estructuras_publicas')
	.factory('constitucion_de_sociedadesService',dataService);

	function dataService ($resource){
		var resource = $resource('/constitucion_de_sociedades/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getconstitucion_de_sociedades:getconstitucion_de_sociedades
		}

		return service;

		function getconstitucion_de_sociedades(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
