(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.estructuras_publicas')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.estructuras_publicas.constitucion_de_sociedades',
		 		config : {
		 			url:'/Constitucion_de_Sociedades',
		 			templateUrl : 'app/client/servicios_notariales/estructuras_publicas/constitucion_de_sociedades/constitucion_de_sociedades.html',
		 			title : 'Constitucion_de_Sociedades',
		 			controller:'constitucion_de_sociedades',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :3,
			 				content : 'Constitución de Sociedades'
		 			}
		 		}
		 	}
	 	];
	 }
}());