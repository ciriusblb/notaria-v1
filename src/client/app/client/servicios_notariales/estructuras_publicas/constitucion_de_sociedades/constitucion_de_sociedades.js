(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.estructuras_publicas')
		.controller('constitucion_de_sociedades',constitucion_de_sociedades);
    constitucion_de_sociedades.$inject = ['constitucion_de_sociedadesService','servicios'];
	function constitucion_de_sociedades(constitucion_de_sociedadesService,servicios){
		var vm = this;
		init();
		console.log("constitucion_de_sociedades ",constitucion_de_sociedadesService );
		function init(){
			constitucion_de_sociedadesService.getconstitucion_de_sociedades().then(function(data){
				vm.constitucion_de_sociedades=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.constitucion_de_sociedades ',vm.constitucion_de_sociedades);
			})
		}
	}	
}());