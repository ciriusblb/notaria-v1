(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.estructuras_publicas')
		.controller('estructura_poderes',estructura_poderes);
    estructura_poderes.$inject = ['estructura_poderesService','servicios'];
	function estructura_poderes(estructura_poderesService,servicios){
		var vm = this;
		init();
		console.log("estructura_poderes ",estructura_poderesService );
		function init(){
			estructura_poderesService.getestructura_poderes().then(function(data){
				vm.estructura_poderes=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.estructura_poderes ',vm.estructura_poderes);
			})
		}
	}	
}());