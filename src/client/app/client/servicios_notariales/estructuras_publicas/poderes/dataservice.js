(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.estructuras_publicas')
	.factory('estructura_poderesService',dataService);

	function dataService ($resource){
		var resource = $resource('/estructura_poderes/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getestructura_poderes:getestructura_poderes
		}

		return service;

		function getestructura_poderes(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
