(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.estructuras_publicas')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.estructuras_publicas.poderes',
		 		config : {
		 			url:'/Poderes',
		 			templateUrl : 'app/client/servicios_notariales/estructuras_publicas/poderes/poderes.html',
		 			title : 'Poderes',
		 			controller:'estructura_poderes',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :2,
			 				content : 'Poderes'
		 			}
		 		}
		 	}
	 	];
	 }
}());