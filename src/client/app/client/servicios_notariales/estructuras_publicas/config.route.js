(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.estructuras_publicas')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.estructuras_publicas',
		 		config : {
		 			url:'/Escrituras_Publicas',
		 			templateUrl : 'app/client/servicios_notariales/estructuras_publicas/estructuras_publicas.html',
		 			children:[],
		 			title : 'Escrituras_Publicas',
		 			go:'compra_venta',
		 			settings : {
			 				nav :1,
			 				content : 'Escrituras Públicas'
		 			}
		 		}
		 	}
	 	];
	 }
}());