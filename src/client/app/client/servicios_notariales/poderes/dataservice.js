(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.poderes')
	.factory('poderesService',dataService);

	function dataService ($resource){
		var resource = $resource('/poderes/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getpoderes:getpoderes
		}

		return service;

		function getpoderes(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
