(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.poderes')
		.controller('poderes',poderes);
    poderes.$inject = ['poderesService','servicios'];
	function poderes(poderesService,servicios){
		var vm = this;
		init();
		function init(){
			poderesService.getpoderes().then(function(data){
				vm.poderes=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;


                console.log('vm.poderes ',data);
			})
		}
	}	
}());