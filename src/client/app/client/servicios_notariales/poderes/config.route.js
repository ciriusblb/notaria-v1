(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.poderes')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.poderes',
		 		config : {
		 			url:'/Poderes',
		 			templateUrl : 'app/client/servicios_notariales/poderes/poderes.html',	 			 
		 			title : 'Poderes',
		 			controller:'poderes',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :6,
			 				content : 'Poderes'
		 			}
		 		}
		 	}
	 	];
	 }
}());