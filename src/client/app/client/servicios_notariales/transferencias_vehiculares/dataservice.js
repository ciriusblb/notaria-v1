(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.transferencias_vehiculares')
	.factory('transferencias_vehicularesService',dataService);

	function dataService ($resource){
		var resource = $resource('/transferencias_vehiculares/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			gettransferencias_vehiculares:gettransferencias_vehiculares
		}

		return service;

		function gettransferencias_vehiculares(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
