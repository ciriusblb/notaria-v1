(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.transferencias_vehiculares')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.transferencias_vehiculares',
		 		config : {
		 			url:'/Transferencias_vehiculares',
		 			templateUrl : 'app/client/servicios_notariales/transferencias_vehiculares/transferencias_vehiculares.html',	 			 
		 			title : 'Transferencias_vehiculares',
		 			controller:'transferencias_vehiculares',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :2,
			 				content : 'Transferencias Vehiculares'
		 			}
		 		}
		 	}
	 	];
	 }
}());