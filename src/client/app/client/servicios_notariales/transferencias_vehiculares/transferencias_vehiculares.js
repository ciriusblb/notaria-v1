(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.transferencias_vehiculares')
		.controller('transferencias_vehiculares',transferencias_vehiculares);
    transferencias_vehiculares.$inject = ['transferencias_vehicularesService','servicios'];
	function transferencias_vehiculares(transferencias_vehicularesService,servicios){
		var vm = this;
		vm.requisitos=[];
		init();
		function init(){
			transferencias_vehicularesService.gettransferencias_vehiculares().then(function(data){
				vm.transferencias_vehiculares=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.transferencias_vehiculares ',vm.requisitos);
			})
		}
	}	
}());