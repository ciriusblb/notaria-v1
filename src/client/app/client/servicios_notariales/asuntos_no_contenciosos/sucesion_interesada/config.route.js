(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.asuntos_no_contenciosos.sucesion_interesada',
		 		config : {
		 			url:'/Sucesion_Interesada',
		 			templateUrl : 'app/client/servicios_notariales/asuntos_no_contenciosos/sucesion_interesada/sucesion_interesada.html',	 			 
		 			title : 'Sucesion_Interesada',
		 			controller:'sucesion_interesada',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :2,
			 				content : 'Sucesion Interesada'
		 			}
		 		}
		 	}
	 	];
	 }
}());