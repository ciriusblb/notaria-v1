(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
		.controller('sucesion_interesada',sucesion_interesada);
    sucesion_interesada.$inject = ['sucesion_interesadaService','servicios'];
	function sucesion_interesada(sucesion_interesadaService,servicios){
		var vm = this;
		init();
		function init(){
			sucesion_interesadaService.getsucesion_interesada().then(function(data){
				vm.sucesion_interesada=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;

                console.log('vm.sucesion_interesada ',data);
			})
		}
	}	
}());