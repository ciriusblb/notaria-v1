(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	.factory('sucesion_interesadaService',dataService);

	function dataService ($resource){
		var resource = $resource('/sucesion_interesada/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getsucesion_interesada:getsucesion_interesada
		}

		return service;

		function getsucesion_interesada(){
		return resource.query().$promise
			.then(function(data){
				console.log('service ',data);
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
