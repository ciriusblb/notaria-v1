(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
		.controller('patrimonio_familiar',patrimonio_familiar);
    patrimonio_familiar.$inject = ['patrimonio_familiarService','servicios'];
	function patrimonio_familiar(patrimonio_familiarService,servicios){
		var vm = this;
		init();
		function init(){
			patrimonio_familiarService.getpatrimonio_familiar().then(function(data){
				vm.patrimonio_familiar=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.patrimonio_familiar ',data);
			})
		}
	}	
}());