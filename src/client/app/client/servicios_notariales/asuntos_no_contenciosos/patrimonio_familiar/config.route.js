(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.asuntos_no_contenciosos.patrimonio_familiar',
		 		config : {
		 			url:'/Patrimonio_Familiar',
		 			templateUrl : 'app/client/servicios_notariales/asuntos_no_contenciosos/patrimonio_familiar/patrimonio_familiar.html',	 			 
		 			title : 'Patrimonio_Familiar',
		 			controller:'patrimonio_familiar',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :3,
			 				content : 'Patrimonio Familiar'
		 			}
		 		}
		 	}
	 	];
	 }
}());