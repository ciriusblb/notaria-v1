(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	.factory('patrimonio_familiarService',dataService);

	function dataService ($resource){
		var resource = $resource('/patrimonio_familiar/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getpatrimonio_familiar:getpatrimonio_familiar
		}

		return service;

		function getpatrimonio_familiar(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
