(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	.factory('divorciosService',dataService);

	function dataService ($resource){
		var resource = $resource('/divorcios/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getdivorcios:getdivorcios
		}

		return service;

		function getdivorcios(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
