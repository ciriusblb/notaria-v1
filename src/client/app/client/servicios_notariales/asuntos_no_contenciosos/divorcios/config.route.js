(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.asuntos_no_contenciosos.divorcios',
		 		config : {
		 			url:'/Divorcios',
		 			templateUrl : 'app/client/servicios_notariales/asuntos_no_contenciosos/divorcios/divorcios.html',	 			 
		 			title : 'Divorcios',
		 			controller:'divorcios',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :4,
			 				content : 'Divorcios'
		 			}
		 		}
		 	}
	 	];
	 }
}());