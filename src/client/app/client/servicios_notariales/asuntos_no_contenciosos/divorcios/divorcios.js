(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
		.controller('divorcios',divorcios);
    divorcios.$inject = ['divorciosService','servicios'];
	function divorcios(divorciosService,servicios){
		var vm = this;
				vm.requisitos=[];

		init();
		function init(){
			divorciosService.getdivorcios().then(function(data){
				vm.divorcios=data;
				vm.requisitos=data[0].requisitos;
				vm.lugar=data[0].lugar;
				vm.nota=data[0].nota;
                console.log('vm.divorcios ',vm.requisitos);
			})
		}
	}	
}());