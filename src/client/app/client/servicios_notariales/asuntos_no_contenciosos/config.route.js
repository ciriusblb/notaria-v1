(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.asuntos_no_contenciosos',
		 		config : {
		 			url:'/Asuntos_no_contenciosos',
		 			templateUrl : 'app/client/servicios_notariales/asuntos_no_contenciosos/asuntos_no_contenciosos.html',	 			 
		 			title : 'Asuntos_no_contenciosos',
		 			children:[],
		 			go:'rectificacion_de_partidas',
		 			settings : {
			 				nav :3,
			 				content : 'Asuntos no Contenciosos'
		 			}
		 		}
		 	}
	 	];
	 }
}());