(function(){
	'use strict';
	 angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios_notariales.asuntos_no_contenciosos.rectificacion_de_partidas',
		 		config : {
		 			url:'/Rectificacion_de_Partidas',
		 			templateUrl : 'app/client/servicios_notariales/asuntos_no_contenciosos/rectificacion_de_partidas/rectificacion_de_partidas.html',	 			 
		 			title : 'Rectificacion_de_Partidas',
		 			controller:'rectificacion_de_partidas',
		 			controllerAs:'vm',
		 			settings : {
			 				nav :1,
			 				content : 'Rectificación de Partidas'
		 			}
		 		}
		 	}
	 	];
	 }
}());