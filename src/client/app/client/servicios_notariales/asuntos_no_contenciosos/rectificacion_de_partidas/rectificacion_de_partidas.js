(function(){
	'use strict';
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
		.controller('rectificacion_de_partidas',rectificacion_de_partidas);
    rectificacion_de_partidas.$inject = ['rectificacion_de_partidasService','servicios'];
	function rectificacion_de_partidas(rectificacion_de_partidasService,servicios){
		var vm = this;
		init();
		function init(){
			rectificacion_de_partidasService.getrectificacion_de_partidas().then(function(data){
				vm.rectificacion_de_partidas=data;
				vm.requisitos = data[0].requisitos;
				vm.lugar = data[0].lugar;
				vm.nota = data[0].nota;

                console.log('vm.rectificacion_de_partidas ',data);
			})
		}
	}	
}());