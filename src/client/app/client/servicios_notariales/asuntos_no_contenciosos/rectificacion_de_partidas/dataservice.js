(function (){
	'use strict'
	angular.module('app.client.servicios_notariales.asuntos_no_contenciosos')
	.factory('rectificacion_de_partidasService',dataService);

	function dataService ($resource){
		var resource = $resource('/rectificacion_de_partidas/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getrectificacion_de_partidas:getrectificacion_de_partidas
		}

		return service;

		function getrectificacion_de_partidas(){
		return resource.query().$promise
			.then(function(data){
				console.log('service ',data);
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
