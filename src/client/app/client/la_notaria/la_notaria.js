(function(){
	angular.module('app.client.la_notaria')
		.controller('la_notaria',la_notaria);

	la_notaria.$inject=['$state','routehelper','$scope'];
	function la_notaria($state,routehelper,$scope){
		var vm = this;
		var routes = routehelper.getSubRoutes('Client');
		vm.cardRoutes = routes[2].children;
		console.log(vm.cardRoutes);
		vm.routeActivated="";
		vm.subRouteActivated="";
		$scope.$watch(function(){
		    return $state.$current.name;
		}, function(newVal, oldVal){
			if(newVal.split('.').length>3){
				vm.routeActivated=newVal.slice(0, -(newVal.split('.')[3].length+1));
				vm.subRouteActivated=newVal;
			}else{
				vm.routeActivated=newVal;
			}
		}) 
	}
}())
