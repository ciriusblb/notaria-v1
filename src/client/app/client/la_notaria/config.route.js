(function(){
	'use strict';
	angular.module('app.client.la_notaria')
		.run(appRun);

	appRun.$inject=['routehelper'];

	function appRun(routehelper){
		return routehelper.configureRoutes(getRoutes());
	}

	function getRoutes(){
		return [
			{
				name:'Client.la_notaria',
				config:{
					url:'/La_Notaria',
					templateUrl:'app/client/la_notaria/la_notaria.html',
					children:[],
					title:'La Notaria',
					controller:'la_notaria',
					controllerAs:'vm',
					settings:{
						nav:3,
						content:'La Notaria <i class="fas fa-angle-down"></i>'
					}
				}
			}
		]
	}

}());