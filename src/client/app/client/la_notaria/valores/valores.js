(function(){
	'use strict';
	angular.module('app.client.la_notaria.mision_vision')
		.controller('valores',valores);
    valores.$inject = ['valoresService','servicios'];
	function valores(valoresService,servicios){
		var vm = this;
		init();
		function init(){
			valoresService.getValores().then(function(data){
				vm.valores=data;
                console.log(vm.valores);
			})
		}
	}	
}());