(function(){
	'use strict';
	 angular.module('app.client.la_notaria.valores')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.la_notaria.valores',
		 		config : {
		 			url:'/Valores',
		 			templateUrl : 'app/client/la_notaria/valores/valores.html',
		 			title : 'Valores',
		 			controller : 'valores',
		 			controllerAs: 'vm',	
		 			settings : {
			 				nav :2,
			 				content : 'Valores'
		 			}
		 		}
		 	}
	 	];
	 }
}());