(function (){
	'use strict'
	angular.module('app.client.la_notaria.valores')
	.factory('valoresService',dataService);

	function dataService ($resource){
		var resource = $resource('/Valores/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true}
		})

		var service = {
			getValores:getValores
		}

		return service;

		function getValores(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
