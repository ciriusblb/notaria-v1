(function(){
	'use strict';
	 angular.module('app.client.la_notaria.mision_vision')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.la_notaria.mision_vision',
		 		config : {
		 			url:'/Mision_Vision',
		 			templateUrl : 'app/client/la_notaria/mision_vision/mision_vision.html',
		 			title : 'Mision Vision',
		 			controller : 'Nosotros',
		 			controllerAs: 'vm',	
		 			settings : {
			 				nav :1,
			 				content : 'Misión y Visión'
		 			}
		 		}
		 	}
	 	];
	 }
}());