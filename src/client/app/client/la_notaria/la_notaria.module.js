(function(){
	'use strict';
	angular.module('app.client.la_notaria',[
		'app.client.la_notaria.mision_vision',
		'app.client.la_notaria.valores',
		'app.client.la_notaria.codigo_de_etica'
		]);
}());