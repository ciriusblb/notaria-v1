(function(){
	'use strict';
	 angular.module('app.client.la_notaria.codigo_de_etica')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.la_notaria.codigo_de_etica',
		 		config : {
		 			url:'/Codigo_de_Etica',
		 			templateUrl : 'app/client/la_notaria/codigo_de_etica/codigo_de_etica.html',
		 			title : 'Codigo_De_etica',
		 			settings : {
			 				nav :7,
			 				content : 'Código de Etica'
		 			}
		 		}
		 	}
	 	];
	 }
}());