(function(){
	angular.module('app.client.home')
		.controller('Home',Home);

	Home.$inject=['dataService','servicios','$filter','openweathermapFactory'];
	function Home(dataService,servicios,$filter,openweathermapFactory){
		var vm = this;
		vm.informe={};	
		vm.informes=[];	
		init();
		function init(){
	      dataService.getHome().then(function(data){
	      	data.pop();
	        vm.slider=data[0];
	        for (var i = 0; i < Object.keys(data[1]).length; i++) {
	        	data[1][i].fecha = new Date(data[1][i].fecha);
	        	vm.informes.push(data[1][i]);
	        }
	        vm.noticias=data[2];
	        vm.presentacion=data[3];
	      })
		}
		vm.showInfome=function(id){
			for (var i = 0; i <  vm.informes.length; i++) {
	     		if(id== vm.informes[i].id){
	     			vm.informe= vm.informes[i];
	     		}  
	     	}
		}	
		vm.oculto=false;
		openweathermapFactory.getWeatherFromLocationByCoordinates({
	        lat:"-12.5933100",
	        lon:"-69.1891300",
	        appid:"c15149633bd1fcee331435b478c46cc7"
	    }).then(function(_data){
	        console.info("weather from location by coordinates", _data);
	    });
	}
}())
