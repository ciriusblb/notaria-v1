(function(){
	'use strict';
	angular.module('app.client.noticias')
		.controller('Noticias',Noticias);
    Noticias.$inject = ['noticiasService','servicios','logger','$state'];
	function Noticias(noticiasService,servicios,logger,$state){
		var vm = this;
        init();
        function init(){
            noticiasService.getNoticias().then(function(data){
                vm.noticias=data;
                // for (var i = 0; i < vm.noticias.length; i++) {
                //     vm.noticias[i].fecha = new Date(vm.noticias[i].fecha);
                // }
            })
        }
	}	
}());
