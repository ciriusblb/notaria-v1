(function(){
    'use strict';
    angular.module('app.admin.preguntas')
        .controller('A-Preguntas',Preguntas);
    Preguntas.$inject = ['A_preguntasService','servicios','logger'];
    function Preguntas(A_preguntasService,servicios,logger){
        var vm = this;
        vm.pregunta=doData();
        vm.preguntas=[];
        vm.buttons={
            new:true
        }  
        vm.updates={};
        vm.nuevo=nuevo;  
        vm.editar=editar;  
        vm.eliminar=eliminar;
        init();

        function init(){
            A_preguntasService.getPreguntas().then(function(data){
                vm.preguntas=data;
                for (var i = 0; i < vm.preguntas.length; i++) {
                    vm.preguntas[i].texto=servicios.textToArray(vm.preguntas[i].respuesta);
                }
            }) 
        }
        vm.seleccionar=function(id){
            var i =servicios.getPosicion(id,vm.preguntas);
            vm.pregunta=JSON.parse(JSON.stringify(vm.preguntas[i]));
            vm.buttons={edit:true,delete:true};
            vm.updates.disabled=true; 
            vm.updates.idSelected=id; 
        }
        vm.guardar = function(formulario){
            if(formulario){
                vm.pregunta.texto=servicios.textToArray(vm.pregunta.respuesta);
                vm.preguntaAux=JSON.parse(JSON.stringify(vm.pregunta));

                if(!vm.pregunta.id){
                    A_preguntasService.savePregunta(vm.pregunta).then(function(data){
                        vm.preguntaAux.id=data.id;
                        vm.preguntas.push(vm.preguntaAux);
                        vm.cancelar();
                    }) 
                }else{
                    A_preguntasService.editPregunta(vm.pregunta).then(function(data){
                        var i =servicios.getPosicion(vm.updates.idSelected,vm.preguntas);
                        vm.preguntas[i]=vm.preguntaAux;
                        vm.cancelar();
                    })
                }
            }else{
                logger.warning('¡Complete los campos!');
            }
        }
        vm.cancelar = function(){
            vm.pregunta=doData();
            vm.buttons={new:true};
            vm.updates.disabled=true;
            vm.updates.idSelected=undefined;
        }
        function eliminar(){
            A_preguntasService.removePregunta({id:vm.pregunta.id}).then(function(data){
                var i =servicios.getPosicion(vm.updates.idSelected,vm.preguntas);
                vm.preguntas.splice(i,1);
                vm.cancelar();
            })
        }
        function nuevo(){
            vm.buttons={new:true};
            vm.updates.disabled=false;
        } 
        function editar(){
            vm.updates.disabled=false;
        }  
        function doData(){
            return {
                pregunta:'',
                respuesta:'',
            } 
        }
    }   
}());