(function(){
	'use strict';
	 angular.module('app.admin.preguntas')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){
	 	routehelper.configureRoutes( getRoutes() );
	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Admin.preguntas',
		 		config : {
		 			url:'/Preguntas_frecuentes',
		 			templateUrl : 'app/admin/preguntas_frecuentes/preguntas_frecuentes.html',
		 			controller : 'A-Preguntas',
		 			controllerAs: 'vm',		 			 
		 			title : 'Preguntas Frecuentes',
		 			settings : {
			 				nav : 4,
			 				content : 'Preguntas Frecuentes'
		 			}

		 		}
		 	}
	 	];
	 }
}());