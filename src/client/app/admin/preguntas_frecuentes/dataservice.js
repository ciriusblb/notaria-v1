(function (){
	'use strict'
	angular.module('app.admin.preguntas')
	.factory('A_preguntasService',dataService);

	function dataService ($resource){
		var resource = $resource('/A_Preguntas/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true},
			'save' : {method: 'POST'},
			'edit' : {method: 'PUT'},
            'remove': { method:'DELETE'}
		})

		var service = {
			getPreguntas:getPreguntas,
			savePregunta:savePregunta,
			editPregunta:editPregunta,
			removePregunta:removePregunta
		}

		return service;

		function getPreguntas(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
		function savePregunta(data){
			return resource.save(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}	
		function editPregunta(data){
			return resource.edit(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}	
		function removePregunta(data){
			return resource.remove(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}			
	}
}())
