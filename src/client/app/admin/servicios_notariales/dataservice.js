(function (){
	'use strict'
	angular.module('app.admin.servicios_notariales')
	.factory('A_servicios_notariales',dataService);

	function dataService ($resource,servicios,$state){
		var resource = $resource('/A_servicios_notariales/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true,skipAuthorization:false},
			'save' : {method: 'POST'},
			'edit' : {method: 'PUT'},
            'remove': { method:'DELETE'}
		})
		var views=[];
		var service = {
			getAllData:getAllData,
			getData:getData,
			saveData:saveData,
			editData:editData,
			removeData:removeData,
			vistas:vistas,
			prueba:prueba,
			limitarVistas:limitarVistas,
			servicio_notarialSelected:undefined,
			updates:{disabled:true,idSelected:undefined}
		}

		return service;

		function getAllData(){
			return resource.query().$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}
		function getData(){
			return resource.get($state.params).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}
		function saveData(data){
			return resource.save(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})	
		}
		function editData(data){
			return resource.edit(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})		
		}
		function removeData(data){
			return resource.remove(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})	
		}
		function limitarVistas(vistas,subvistas){
			var mainVistas = service.vistas();
			for (var i = 0; i < mainVistas.length; i++) {
				const resultado = vistas.find( resp => resp.vista === mainVistas[i].vista );
				if(resultado && resultado.vista==mainVistas[i].vista){
					if(!mainVistas[i].subvistas){
						delete mainVistas[i];
					}else{
						for (var j = 0; j < mainVistas[i].subvistas.length; j++) {
							const subresultado = subvistas[0].find( resp => resp.subvista == mainVistas[i].subvistas[j].subvista );
							if(subresultado &&  subresultado.subvista == mainVistas[i].subvistas[j].subvista){
								delete mainVistas[i].subvistas[j];
							}
						}
						mainVistas[i].subvistas=mainVistas[i].subvistas.filter(Boolean);
						if(mainVistas[i].subvistas.length==0){
							delete mainVistas[i];
						}
					}
				}
			}
			views = mainVistas.filter(Boolean);			
        	return views;
		}
		function vistas(){
			return [
	            {
	                vista:"Escrituras Públicas",
	                subvistas:[
	                    {subvista:"Compra Venta"},
	                    {subvista:"Poderes"},
	                    {subvista:"Constitucion de Sociedades"},
	                    {subvista:"Constitucion de Hipotecas"}
	                ]
	            },
	            {vista:"Transferencias Vehiculares"},
	            {
	                vista:"Asuntos no Contenciosos",
	                subvistas:[
	                    {subvista:"Rectificación de Partidas"},
	                    {subvista:"Sucesión Interesada"},
	                    {subvista:"Patrimonio Familiar"},
	                    {subvista:"Divorcios"}
	                ]
	            },
	            {vista:"Protestos"},
	            {vista:"Cartas Notariales"},
	            {vista:"Poderes"},
	            {
	                vista:"Legalizaciones",
	                subvistas:[
	                    {subvista:"De firmas"},
	                    {subvista:"De copias"}
	                ]
	            }
        	]
		}
		function prueba(){
			return views;
			// return new Promise(function(resolve, reject) {
	            // let intervalo = setInterval( () => {
			          // resolve(views)
			          // clearInterval(intervalo);
			    // }, 500);
		    // });
		}
	}
}())