(function(){
	'use strict';
	 angular.module('app.admin.servicios_notariales')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name:'Admin.servicios_notariales.servicio_notarial',
				config:{
					url:'/:id',
					templateUrl:'app/admin/servicios_notariales/servicio_notarial/servicio_notarial.html',
					title:'Servicio Notarial',
					controller : 'A-servicio_notarial',
		 			controllerAs: 'vm'
				}
		 	}
	 	];
	 }
}());