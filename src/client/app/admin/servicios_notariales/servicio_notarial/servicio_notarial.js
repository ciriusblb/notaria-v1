(function(){
	'use strict';
	angular.module('app.admin.servicios_notariales')
		.controller('A-servicio_notarial',servicio_notarial);
    servicio_notarial.$inject = ['logger','servicios','A_servicios_notariales','$state','$rootScope'];
	function servicio_notarial(logger,servicios,A_servicios_notariales,$state,$rootScope){
        var vm=this;
        vm.vistas=[];
        vm.enable=true;
        vm.buttons={
            new:true
        }  
        vm.updates=A_servicios_notariales.updates;
        vm.nuevo=nuevo;  
        vm.editar=editar;  
        vm.eliminar=eliminar;
        vm.eliminarDato=eliminarDato;
        vm.idxVista=false;
        function getvistas(){
            vm.vistas=A_servicios_notariales.vistas();
            console.log(vm.vistas);
        }
        vm.servicio_notarial =doData();

        function init(){
            A_servicios_notariales.getData().then(function(data){
                vm.selectVista(data.vista);
                vm.servicio_notarial=data;
                vm.servicio_notarial.eliminarId=[];
                vm.eliminarServicio={
                    eliminarId:[]
                }
                console.log('vm.servicio_notarial ',vm.servicio_notarial);
                for (var i = 0; i < vm.servicio_notarial.requisitos.length; i++) {
                    vm.eliminarServicio.eliminarId.push(vm.servicio_notarial.requisitos[i].id);
                }
                vm.eliminarServicio.id=vm.servicio_notarial.id;
                vm.buttons={edit:true,delete:true};
            })
        }
        vm.agregarrequisito = function(){
            var requisito = {id:vm.servicio_notarial.requisitos.length,requisito:'',agregar:1};
            vm.servicio_notarial.requisitos.push(requisito);
        }
        function eliminarDato(idx){
            if(vm.servicio_notarial.id && vm.servicio_notarial.requisitos[idx].id_vista){
                vm.servicio_notarial.eliminarId.push(vm.servicio_notarial.requisitos[idx].id);
            }
            vm.servicio_notarial.requisitos.splice(idx,1);
        }
        function nuevo(){
            vm.enable=false;
            vm.buttons={new:true};
            vm.updates.disabled=false;
            A_servicios_notariales.updates.disabled=false;
            vm.vistas=A_servicios_notariales.prueba();
            // A_servicios_notariales.prueba().then(function(data){
            //     vm.vistas=data;
            //     console.log(vm.vistas);
            // });
        }
        function editar(){
            vm.updates.disabled=false;
            vm.enable=true;
            A_servicios_notariales.updates.disabled=false;
        }
        function eliminar(){
            A_servicios_notariales.removeData(vm.eliminarServicio).then(function(data){
                A_servicios_notariales.servicio_notarialSelected=vm.servicio_notarial;
                vm.cancelar();
                $rootScope.$broadcast('servicios_notariales','eliminado');
            })
        }
        vm.guardar = function(form){
            if(form){
                vm.servicio_notarial.cantidadRequisitos=vm.servicio_notarial.requisitos.length;
                if(!vm.servicio_notarial.id){
                    A_servicios_notariales.saveData(vm.servicio_notarial).then(function(data){
                        vm.servicio_notarial.id=data.id;
                        A_servicios_notariales.servicio_notarialSelected=vm.servicio_notarial;
                        vm.cancelar();
                        $rootScope.$broadcast('servicios_notariales','nuevo');
                    })
                }else{
                    console.log(vm.servicio_notarial);
                    A_servicios_notariales.editData(vm.servicio_notarial).then(function(data){
                        A_servicios_notariales.servicio_notarialSelected=vm.servicio_notarial;
                        vm.cancelar();
                        $rootScope.$broadcast('servicios_notariales','editado');
                    })                    
                }
            }else{
                logger.warning('¡Complete todos los campos!');
            }
        }
        vm.selectVista=function(vista){
            vm.servicio_notarial.subvista="";
            vm.idxVista=false;
            for (var i = 0; i < vm.vistas.length; i++) {
                if(vm.vistas[i].vista==vista && vm.vistas[i].subvistas){
                    vm.subvistas=vm.vistas[i].subvistas;
                    vm.idxVista=true;
                    break;
                }
            }
            return vm.subvistas;            
        }
        vm.cancelar = function(){
            vm.servicio_notarial =doData();
            vm.buttons={new:true};
            vm.updates.disabled=true;
            A_servicios_notariales.updates.disabled=true;
            A_servicios_notariales.updates.idSelected=undefined;
            $state.go('Admin.servicios_notariales.servicio_notarial', {id:0});    
        }
        function doData(){
            return {
                vista:'',
                subvista:'',
                requisitos:[
                    {
                        id:0,
                        requisito:''
                    }
                ],
                lugar:'',
                nota:''
            }
        }
        if($state.params.id!="0"){
            vm.vistas=A_servicios_notariales.vistas();
            init();
        }
	}	
}());