(function(){
	'use strict';
	angular.module('app.admin.servicios_notariales')
		.controller('A-servicios_notariales',servicios_notariales);
    servicios_notariales.$inject = ['logger','servicios','A_servicios_notariales','$state','$rootScope'];
	function servicios_notariales(logger,servicios,A_servicios_notariales,$state,$rootScope){
        var vm = this;
        init();
        vm.updates=A_servicios_notariales.updates;

        vm.selected=[{selected:true}];
        vm.props=['subvista'];
        vm.request={};

        $rootScope.$on('servicios_notariales',function(event, action){
            if(A_servicios_notariales.servicio_notarialSelected){
                var i = servicios.getPosicion(A_servicios_notariales.servicio_notarialSelected.id,vm.servicios_notariales);
                switch(action){
                    case 'nuevo': vm.servicios_notariales.push(A_servicios_notariales.servicio_notarialSelected);break;
                    case 'editado': vm.servicios_notariales[i]=A_servicios_notariales.servicio_notarialSelected;break;
                    case 'eliminado': vm.servicios_notariales.splice(i,1);break;
                }
                reinit();
                A_servicios_notariales.servicio_notarialSelected=undefined;
                logger.success('Enlace '+action+'!');
            }
        });
        function init(){
            A_servicios_notariales.getAllData().then(function(data){
                vm.servicios_notariales=data;
                vm.items=vm.servicios_notariales;
                vm.vistas=servicios.doNewArray(vm.items,'vista');
                vm.tofilter=servicios.doNewArrays(vm.items,vm.props);
                A_servicios_notariales.limitarVistas(vm.vistas,vm.tofilter);
                 $state.go('Admin.servicios_notariales.servicio_notarial', {id:0});  

            });
        }

        vm.select = function(id){
            if(A_servicios_notariales.updates.disabled){
                vm.updates=A_servicios_notariales.updates={idSelected:id,disabled:true};
                $state.go('Admin.servicios_notariales.servicio_notarial', {id:id});  
            }
        }
        vm.filtro=function(array,idx,ind,prop,word){
            vm.request[prop]=word;
            vm.selected[ind].selected=false;
            array.filter(function(item){ 
              item.selected=false;
            });
            array[idx].selected=true;
            vm.items=servicios.mainFilter(vm.servicios_notariales,vm.request);
            vm.tofilter=servicios.doNewArrays(vm.items,vm.props);
            return vm.items;
        }
        vm.filtro2 = function(idx,i){
            vm.ind=i;
            vm.tofilter[i][idx].selected=!vm.tofilter[i][idx].selected;
            vm.items=servicios.secondaryFilter(vm.servicios_notariales,vm.request,vm.tofilter,vm.props);
            vm.tofilter=servicios.verifyFilters(vm.items,vm.tofilter,vm.props,i);
            return vm.items;
        }
        vm.reset= function(array,i,prop){
            delete vm.request[prop];
            array.filter(function(item){ 
              item.selected=false;
            });
            vm.items=servicios.mainFilter(vm.servicios_notariales,vm.request);
            vm.tofilter=servicios.doNewArrays(vm.items,vm.props);
            return vm.items;
        }
        function reinit(){
            vm.vistas=servicios.doNewArray(vm.servicios_notariales,'vista');
            for (var i = 0; i < vm.vistas.length; i++) {
                if(vm.vistas[i].vista==vm.request.vista){
                    vm.vistas[i].selected=true;
                }
            }
            vm.items=servicios.secondaryFilter(vm.servicios_notariales,vm.request,vm.tofilter,vm.props);
            vm.tofilter=servicios.doNewArrays(vm.servicios_notariales,vm.props);
            vm.tofilter=servicios.verifyFilters(vm.items,vm.tofilter,vm.props,-1);
            vm.updates=A_servicios_notariales.updates;
            vm.selected[0].selected= vm.items.length === 0 ? true:false;
            vm.items= vm.items.length === 0 ? vm.servicios_notariales:vm.items;
            A_servicios_notariales.limitarVistas(vm.vistas,vm.tofilter);
        }
	}	
}());



