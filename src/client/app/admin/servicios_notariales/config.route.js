(function(){
	'use strict';
	angular.module('app.admin.servicios_notariales')
		.run(appRun);

	appRun.$inject=['routehelper'];

	function appRun(routehelper){
		return routehelper.configureRoutes(getRoutes());
	}

	function getRoutes(){
		return [
			{
				name:'Admin.servicios_notariales',
				config:{
					url:'/Servicios_Notariales',
					templateUrl:'app/admin/servicios_notariales/servicios_notariales.html',
					title:'Servicios_Notariales',
					controller : 'A-servicios_notariales',
		 			controllerAs: 'vm',
					settings:{
						nav:2,
						content:'Servicios_Notariales '
					}
				}
			}
		]
	}

}());