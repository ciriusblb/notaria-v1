(function(){
	'use strict';
	angular.module('app.admin.home.presentacion')
		.controller('A-Presentacion',Presentacion);
    Presentacion.$inject = ['A_presentacionService','servicios','logger','$state','$rootScope'];
	function Presentacion(A_presentacionService,servicios,logger,$state,$rootScope){
		var vm = this
        vm.presentacion=doData();
        vm.buttons={
            new:true
        }  
        vm.updates={};
        vm.nuevo=nuevo;  
        vm.editar=editar;  
        vm.eliminar=eliminar;
        vm.eliminarimagenes=[];
        vm.presentaciones=[];
        init();
        function init(){
        	A_presentacionService.getPresentaciones().then(function(data){
        		vm.presentaciones=data;
                // for (var i = 0; i < vm.presentaciones.length; i++) {
                //     vm.presentaciones[i].texto=servicios.textToArray(vm.presentaciones[i].descripcion);
                // }
        		console.log(vm.presentaciones);
        	});
        }
        vm.select = function(id){
            vm.idSelected=id;
            var i =servicios.getPosicion(id,vm.presentaciones);
            vm.presentacion=JSON.parse(JSON.stringify(vm.presentaciones[i]));
            vm.eliminarPresentacion={
                eliminarimagenes:[],
                eliminarId:[]
            }
            vm.eliminarPresentacion.eliminarimagenes.push(vm.presentacion.presentacion)
            vm.eliminarPresentacion.id=vm.presentacion.id;
            vm.buttons={edit:true,delete:true};
        }
        function nuevo(){
            vm.buttons={new:true};
            vm.updates.disabled=false;
        }
        function editar(){
            vm.updates.disabled=false;
        }
        function eliminar(){
            A_presentacionService.removePresentacion(vm.eliminarPresentacion).then(function(data){
                var i =servicios.getPosicion(vm.idSelected,vm.presentaciones);
                vm.presentaciones.splice(i,1);
                vm.cancelar();
            })
        }
        vm.guardar = function(form){
            if(vm.presentacion.presentacion0 || vm.presentacion.presentacion){
                // vm.presentacion.texto=servicios.textToArray(vm.presentacion.descripcion);
                vm.presentacionAux=JSON.parse(JSON.stringify(vm.presentacion));
                servicios.removeBlob(vm.presentacion);
                if(!vm.presentacion.id){
                    A_presentacionService.savePresentacion(vm.presentacion).then(function(data){
                        vm.presentacionAux.id=data.id;
                        vm.presentacionAux.presentacion=data.presentacion;
                        vm.presentaciones.push(vm.presentacionAux);
                        vm.cancelar();
                    })
                }else{
                    vm.presentacion.eliminarimagenes=vm.eliminarimagenes;
                    A_presentacionService.editPresentacion(vm.presentacion).then(function(data){
                        vm.presentacionAux.presentacion=data.presentacion;
                        // vm.presentacionAux.texto=servicios.textToArray(vm.presentacionAux.descripcion);
                        var i =servicios.getPosicion(vm.idSelected,vm.presentaciones);
                        vm.presentaciones[i]=vm.presentacionAux;
                        vm.cancelar();
                    })                    
                }
            }else{
                logger.warning('¡Complete todos los campos!');
            }
        }
        vm.cancelar = function(){
            vm.presentacion =doData();
            vm.buttons={new:true};
            vm.updates.disabled=true;
            vm.eliminarimagenes=[];
        }
        function doData(){
            return {
                notaria:'',
                titulo:'',
                descripcion:'',
                presentacionBlob:''
            }
        }
	}	
}());

