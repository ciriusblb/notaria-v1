(function (){
	'use strict'
	angular.module('app.admin.home.presentacion')
	.factory('A_presentacionService',dataService);

	function dataService ($resource,$state,servicios){
		var resource = $resource('/A_Presentacion/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true,skipAuthorization:false},
			'save' : {method: 'POST',transformRequest:servicios.transformData,headers:{'Content-Type':undefined}},
			'edit' : {method: 'PUT',transformRequest:servicios.transformData,headers:{'Content-Type':undefined}},
            'remove': { method:'DELETE'}
		})
		var service ={
			getPresentaciones:getPresentaciones,
			getPresentacion:getPresentacion,
			savePresentacion:savePresentacion,
			editPresentacion:editPresentacion,
			removePresentacion:removePresentacion,

			PresentacionSelected:undefined,
            updates:{disabled:true,idSelected:undefined}
		}
		return service;

		function getPresentaciones(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
		function getPresentacion(){
			return resource.get($state.params).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}
		function savePresentacion(data){
		return resource.save(data).$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
		function editPresentacion(data){
		return resource.edit(data).$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
		function removePresentacion(data){
		return resource.remove(data).$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
	}
}())
