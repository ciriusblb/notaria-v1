(function(){
	'use strict';
	 angular.module('app.admin.home.presentacion')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){
	 	routehelper.configureRoutes( getRoutes() );
	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Admin.home.presentacion',
		 		config : {
		 			url:'/Presentacion',
		 			templateUrl : 'app/admin/home/presentacion/presentacion.html',
		 			controller : 'A-Presentacion',
		 			controllerAs: 'vm',		 			 
		 			title : 'Presentacion',
		 			settings : {
			 				nav : 5,
			 				content : 'Presentación'
		 			}

		 		}
		 	}
	 	];
	 }
}());