(function(){
	'use strict';
	angular.module('app.admin',[
		'app.admin.home',
		'app.admin.servicios_notariales',
		'app.admin.la_notaria',
		'app.admin.preguntas'
		]);
}());
