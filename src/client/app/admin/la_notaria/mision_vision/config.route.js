(function(){
	'use strict';
	 angular.module('app.admin.la_notaria.nosotros')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){
	 	routehelper.configureRoutes( getRoutes() );
	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Admin.la_notaria.nosotros',
		 		config : {
		 			url:'/mision_vision',
		 			templateUrl : 'app/admin/la_notaria/mision_vision/nosotros.html',
		 			controller : 'A-Nosotros',
		 			controllerAs: 'vm',		 			 
		 			title : 'Misión y Visión',
		 			settings : {
			 				nav : 1,
			 				content : 'Misión y Visión'
		 			}

		 		}
		 	}
	 	];
	 }
}());