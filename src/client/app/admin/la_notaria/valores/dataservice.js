(function (){
	'use strict'
	angular.module('app.admin.la_notaria.valores')
	.factory('A_valoresService',dataService);

	function dataService ($resource){
		var resource = $resource('/A_Valores/:id',{id:'@id'},{
			'query' : {method: 'GET',isArray:true},
			'save' : {method: 'POST'},
			'edit' : {method: 'PUT'},
            'remove': { method:'DELETE'}
		})

		var service = {
			getvalores:getvalores,
			savevalor:savevalor,
			editvalor:editvalor,
			removevalor:removevalor
		}

		return service;

		function getvalores(){
		return resource.query().$promise
			.then(function(data){
				return data;
			})
			.catch(function(error){
				console.log(error);
			})
		}
		function savevalor(data){
			return resource.save(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}	
		function editvalor(data){
			return resource.edit(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}	
		function removevalor(data){
			return resource.remove(data).$promise
				.then(function(data){
					return data;
				})
				.catch(function(error){
					console.log(error);
				})
		}			
	}
}())
