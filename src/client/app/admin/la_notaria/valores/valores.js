(function(){
	'use strict';
	angular.module('app.admin.la_notaria.valores')
		.controller('A-valores',valores);
    valores.$inject = ['A_valoresService','servicios','logger'];
	function valores(A_valoresService,servicios,logger){
		var vm = this;
		vm.valor=doData();
		vm.buttons={
            new:true
        }  
        vm.updates={};
        vm.nuevo=nuevo;  
        vm.editar=editar;  
        vm.eliminar=eliminar;
        init();

        function init(){
            A_valoresService.getvalores().then(function(data){
                vm.valores=data;
                // for (var i = 0; i < vm.valores.length; i++) {
                //     vm.valores[i].texto=servicios.textToArray(vm.valores[i].descripcion);
                // }
            }) 
        }
        vm.seleccionar=function(id){
            var i =servicios.getPosicion(id,vm.valores);
            vm.valor=JSON.parse(JSON.stringify(vm.valores[i]));
            vm.buttons={edit:true,delete:true};
            vm.updates.disabled=true; 
            vm.updates.idSelected=id; 
        }
        vm.guardar = function(formulario){
            if(formulario){
                // vm.valor.texto=servicios.textToArray(vm.valor.descripcion);
                vm.valorAux=JSON.parse(JSON.stringify(vm.valor));

                if(!vm.valor.id){
                    A_valoresService.savevalor(vm.valor).then(function(data){
                        vm.valorAux.id=data.id;
                        vm.valores.push(vm.valorAux);
                        vm.cancelar();
                    }) 
                }else{
                    A_valoresService.editvalor(vm.valor).then(function(data){
                        var i =servicios.getPosicion(vm.updates.idSelected,vm.valores);
                        vm.valores[i]=vm.valorAux;
                        vm.cancelar();
                    })
                }
            }else{
                logger.warning('¡Complete los campos!');
            }
        }
        vm.cancelar = function(){
            vm.valor=doData();
            vm.buttons={new:true};
            vm.updates.disabled=true;
            vm.updates.idSelected=undefined;
        }
        function eliminar(){
            A_valoresService.removevalor({id:vm.valor.id}).then(function(data){
                var i =servicios.getPosicion(vm.updates.idSelected,vm.valores);
                vm.valores.splice(i,1);
                vm.cancelar();
            })
        }
        function nuevo(){
            vm.buttons={new:true};
            vm.updates.disabled=false;
        } 
        function editar(){
            vm.updates.disabled=false;
        }  
        function doData(){
            return {
                titulo:'',
                descripcion:'',
            } 
        }
	}	
}());