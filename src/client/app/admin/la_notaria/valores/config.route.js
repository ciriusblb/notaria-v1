(function(){
	'use strict';
	 angular.module('app.admin.la_notaria.valores')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){
	 	routehelper.configureRoutes( getRoutes() );
	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Admin.la_notaria.valores',
		 		config : {
		 			url:'/valores',
		 			templateUrl : 'app/admin/la_notaria/valores/valores.html',
		 			controller : 'A-valores',
		 			controllerAs: 'vm',		 			 
		 			title : 'Valores',
		 			settings : {
			 				nav : 2,
			 				content : 'Valores'
		 			}

		 		}
		 	}
	 	];
	 }
}());