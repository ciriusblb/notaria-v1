(function(){
	'use strict';
	angular.module('app.admin.la_notaria')
		.run(appRun);

	appRun.$inject=['routehelper'];

	function appRun(routehelper){
		return routehelper.configureRoutes(getRoutes());
	}

	function getRoutes(){
		return [
			{
				name:'Admin.la_notaria',
				config:{
					url:'/La_notaria',
					templateUrl:'app/admin/la_notaria/la_notaria.html',
		 			children:[],
					title:'La Notaria',
					settings:{
						nav:3,
						content:'La Notaria <i class="fa fa-chevron-down"></i>'
					}
				}
			}
		]
	}

}());