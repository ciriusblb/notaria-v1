(function(){
	'use strict';

	angular.module('app.core',[
		'ngSanitize',
		'angular-jwt',
		'blocks.router',
		'blocks.logger',
		'blocks.exception',
		// 'ngResource',
		'app.data',
		'ui.router',
		'slickCarousel',
		'ui.bootstrap',
		'satellizer',
		'jtt_openweathermap'

		]);



}());