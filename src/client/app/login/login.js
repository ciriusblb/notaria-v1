(function(){
	angular.module('app.login')
		.controller('Login',Login);

	Login.$inject=['$state','oneService','$scope'];
	function Login($state,oneService,$scope){
		var vm = this;
		vm.slickConfig = {
            dots: true,
            infinite: true,
            slidesToShow:1,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };
        $state.go("Login.usuario");
	}
}())

