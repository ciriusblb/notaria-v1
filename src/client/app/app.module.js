(function(){
	'use strict';

	angular.module('app.main',[
		'app.core',
		'app.widgets',
		'app.layout',
			
		'app.client',
		'app.login',
		'app.admin'
		])
		.config(['$authProvider','$httpProvider','jwtInterceptorProvider','jwtOptionsProvider','TOKEN_KEY',function ($authProvider,$httpProvider,jwtInterceptorProvider,jwtOptionsProvider,TOKEN_KEY){
			$authProvider.loginUrl = '/login';
			$authProvider.tokenName = "Token";
			$authProvider.tokenPrefix = "myApp";

			jwtInterceptorProvider.tokenGetter = function(){
				return window.localStorage.getItem(TOKEN_KEY.myToken);
			}
			jwtOptionsProvider.config({
		      	whiteListedDomains: ['https://notaria-v1.herokuapp.com/', 'localhost']
		    });
			$httpProvider.interceptors.push("jwtInterceptor");
		}])
		.run(function($rootScope,$state,oneService,AUTH_EVENTS){
			$rootScope.$on('$stateChangeStart',function(event, next, nextParams, fromState){
				if (oneService.Autenticated() === 'error') {
					if (next.name === 'Admin') {
						event.preventDefault();
						$state.go('Login.usuario');
					}
				}else{
					if (next.name === 'Login.usuario' || next.name === 'Login.password') {
						event.preventDefault();
						$state.go('Admin');
					}
				}
			})
		});
}());

