-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-12-2018 a las 17:14:39
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `notaria_db`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAll` ()  BEGIN
select * from `h_slide` order by id desc;
select * from `h_informe` order by id desc;
select * from `h_noticia` order by id desc;
select * from `h_presentacion` order by id desc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `spEditServicioNotarial` (IN `spRequisito` LONGTEXT, IN `spId` INTEGER(11), IN `spId_Vista` INTEGER(11), IN `spAgregar` BOOLEAN)  BEGIN
if spAgregar = TRUE then
   insert into h_requisito(requisito,id_vista) values(spRequisito,spId_Vista);
ELSE
    update h_requisito SET requisito = spRequisito where id = spId;
end if;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_admin`
--

CREATE TABLE `h_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` longtext NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_admin`
--

INSERT INTO `h_admin` (`id`, `username`, `password`, `type`) VALUES
(1, 'admin', 'admin', 'contenido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_enlace`
--

CREATE TABLE `h_enlace` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `enlace` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_enlace`
--

INSERT INTO `h_enlace` (`id`, `titulo`, `enlace`, `url`) VALUES
(1, 'Diario oficial del vicentenario El peruano', 'enlace0001.jpg', 'http://elperuano.pe/'),
(2, 'Organismo Oficial de las contrataciones del Estado', 'enlace0002.jpg', 'http://portal.osce.gob.pe/osce/'),
(3, 'La contraloria general de  la republica', 'enlace0003.jpg', 'http://www.contraloria.gob.pe/wps/wcm/connect/cgrn'),
(4, 'Junta de decanos de los colegios de notarios del perú', 'enlace0004.jpg', 'http://www.juntadedecanos.org.pe/'),
(5, 'Revista de investigación y negocios', 'enlace0005.jpg', 'http://aempresarial.com/web/index.php#&panel1-1'),
(6, 'Revista de Asesoria Especialida', 'enlace0006.jpg', 'http://www.asesorempresarial.com/web/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_informe`
--

CREATE TABLE `h_informe` (
  `id` int(11) NOT NULL,
  `header` varchar(200) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` longtext NOT NULL,
  `informe` varchar(50) NOT NULL,
  `nombre_archivo` varchar(100) NOT NULL,
  `fecha` varchar(500) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_informe`
--

INSERT INTO `h_informe` (`id`, `header`, `titulo`, `descripcion`, `informe`, `nombre_archivo`, `fecha`, `fecha_registro`) VALUES
(1, 'Comunicado 1', 'Título 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam', 'informe0001.docx', 'informe notaria garcia medina.docx', 'Fri Nov 09 2018 00:00:00 GMT-0500 (hora estándar de Perú)', '2018-11-09 14:20:52'),
(2, 'Comunicado 2', 'Título 2', 'Segundo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam', 'informe0002.pdf', '1051_TecnicasIISimulacion.pdf', 'Thu Nov 08 2018 00:00:00 GMT-0500 (hora estándar de Perú)', '2018-11-09 14:22:21'),
(3, 'Comunicado 3', 'Título 3', 'Tercer Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam', 'informe0003.docx', 'resumen metricas.docx', 'Wed Nov 07 2018 00:00:00 GMT-0500 (hora estándar de Perú)', '2018-11-09 14:24:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_nosotros`
--

CREATE TABLE `h_nosotros` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `descripcion` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_nosotros`
--

INSERT INTO `h_nosotros` (`id`, `titulo`, `descripcion`) VALUES
(1, 'Misión', 'Brindamos seguridad jurídica de confianza, oportuna, con soluciones integrales, innovadoras, de calidad, actualizadas, personalizadas, con una comunicación permanente, con responsabilidad social y profesionalismo.'),
(3, 'Visión', 'Ser la Notaría líder a nivel nacional, ofreciendo a nuestros usuarios plena satisfacción en el servicio prestado.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_noticia`
--

CREATE TABLE `h_noticia` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` longtext NOT NULL,
  `noticia` varchar(50) NOT NULL,
  `fuente` varchar(200) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_noticia`
--

INSERT INTO `h_noticia` (`id`, `titulo`, `descripcion`, `noticia`, `fuente`, `fecha`) VALUES
(1, 'Noticia 1', 'Estos son nuestros servicios', 'noticia0001.jpg', 'http://localhost:4000/#/Client/Servicios_notariales/Estructuras_Publicas/Compra_Venta', '2018-08-15 15:41:36'),
(2, 'Noticia 2', 'Solicitelosvia web', 'noticia0002.jpg', 'https://www.notariapaino.com.pe/tramites-en-linea/', '2018-08-15 15:43:22'),
(3, 'Noticia 3', 'La manera mas efeciva de revisar el estado de sus trámites', 'noticia0003.jpg', 'https://www.notariapaino.com.pe/servicios-en-linea/consultas-en-linea/buscar-por-numero-dni/', '2018-08-15 15:45:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_preguntas`
--

CREATE TABLE `h_preguntas` (
  `id` int(11) NOT NULL,
  `pregunta` longtext NOT NULL,
  `respuesta` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_preguntas`
--

INSERT INTO `h_preguntas` (`id`, `pregunta`, `respuesta`) VALUES
(1, 'Pregunta 01', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(3, 'Pregunta 2', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.'),
(6, 'Pregunta 2', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fug');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_presentacion`
--

CREATE TABLE `h_presentacion` (
  `id` int(11) NOT NULL,
  `notaria` varchar(300) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `descripcion` longtext NOT NULL,
  `presentacion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_presentacion`
--

INSERT INTO `h_presentacion` (`id`, `notaria`, `titulo`, `descripcion`, `presentacion`) VALUES
(12, 'Dra: Lourdes Madeleine García Medina', 'Presentación', 'Bienvenidos a la Portal Web Oficial de la Notaría García Medina.', 'presentacion0001.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_requisito`
--

CREATE TABLE `h_requisito` (
  `id` int(11) NOT NULL,
  `requisito` longtext NOT NULL,
  `id_vista` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_requisito`
--

INSERT INTO `h_requisito` (`id`, `requisito`, `id_vista`) VALUES
(1, 'Minuta debidamente firmada por todas las partes y autorizada por abogado colegiado.', 1),
(2, 'Proporcionar generales de ley de los contratantes, llenando la hoja de datos que entregamos en el mostrador.', 1),
(3, 'Acompañar libros de Actas en caso de contratos de sociedades mercantiles o civiles. En caso de asociaciones civiles, acompañar además, libro padrón de socios.', 1),
(4, 'Adjuntar fotocopia de los documentos de identidad (DNI, CIP, Carnet de Extranjería, Etc) de los contratantes. El DNI debe contar con el sello de la última votación, con el de dispensa o con el pago de multa.', 1),
(5, 'Si corresponde, y para una mejor revisión, adjuntar copia vigente de la partida registral de propiedad Inmueble, del Registro de Personas Jurídicas o la que se requiera según sea el caso.', 1),
(6, 'Si se trata de contratos de tráfico patrimonial inmobiliario adjuntar copia del pago del impuesto de alcabala, de la declaración jurada de inmobiliario del predio (hojas HR y PU) y del pago del impuesto predial de todo el año en curso.', 1),
(7, 'Si se requiere constancia o fe de entrega notarial del dinero, indicar previamente datos del cheque. Cuando se trate de bienes de propiedad común de los cónyuges, se requiere la intervención de ambos por propio derecho o por poder.', 1),
(8, 'Solicitud debidamente firmada o minuta suscrita por las partes y autorizada por abogado colegiado.', 2),
(9, 'Proporcionar generales de ley del solicitante o de los contratantes.', 2),
(10, 'Adjuntar fotocopia de documentos de identidad vigentes (DNI, CIP, Carnet de Extranjería, Etc.) del solicitante o contratantes.', 2),
(11, 'Cancelar importe de las publicaciones, cuando corresponda.', 2),
(12, 'Adjuntar copias originales de las partidas del Registro Civil que correspondan.', 2),
(13, 'Proporcionar copia de la tarjeta de propiedad. Presentar original y copia de los documentos de identidad vigentes (DNI, CIP, Carnet de Extranjería, Etc.) de los contratantes, el DNI deberá contar con la constancia de votación al día, dispensa o pago de multa.', 3),
(14, 'En caso de personas jurídicas, presentar vigencia de poderes.', 3),
(15, 'En caso de vehículo con una antigüedad de in matriculación no mayor a 3 años, presentar el debido comprobante que acredite el pago del impuesto al patrimonio vehicular.', 3),
(16, 'Presentar original y copia del Seguro Obligatorio de Accidentes de Tránsito (SOAT) vigente.', 3),
(17, 'Cuando se trate de un vehículo de propiedad común de los cónyuges, se requiere la intervención de ambos por propio derecho o poder.', 3),
(18, 'Nombre del causante.', 4),
(19, 'Indicación del último domicilio del causante.', 4),
(20, 'Copia certificada de la partida de defunción o de la declaración judicial de muerte presunta.', 4),
(21, 'Copia certificada de la partida de nacimiento del presunto heredero o herederos, o documento público que contenga el reconocimiento o la declaración judicial, si se trata de hijo extra-matrimonial o adoptivo.', 4),
(22, 'Partida de matrimonio o la inscripción en el Registro Personal de la declaración de la unión de hecho, adjuntándose, según sea el caso, el testimonio de la escritura pública o la copia certificada de la sentencia judicial firme', 4),
(23, 'Relación de los bienes conocido.', 4),
(24, 'Certificación Registral en la que conste que no hay inscrito testamento u otro proceso de sucesión intestada; en el lugar del último domicilio del causante y en aquel donde hubiera tenido bienes inscritos.', 4),
(25, 'Copia del documento de identidad del solicitante.', 4),
(26, 'Minuta suscrita por las partes y autorizada por abogado colegiado, la minuta deberá contener la declaración expresa de no tener deudas pendientes de pago, individualizando el predio que se propone afectar y a los respectivos beneficiarios.', 5),
(27, 'Partidas que acrediten el vínculo entre el solicitante y los beneficiarios.', 5),
(28, 'Copia literal del inmueble actualizada y certificado de cargas y gravámenes del predio.', 5),
(29, 'Presentar debidamente firmada la carta en original y dos copias.', 6),
(30, 'Indicar claramente dirección, nombre o razón social del destinatario.', 6),
(31, 'Presentarse a la notaria con el documento original que se pretende legalizar', 7),
(32, 'Presentar copia exacta y legible del documento original.', 7),
(33, 'Presentarse a la notaria con el documento original que se pretende legalizar', 8),
(34, 'Documento de identidad vigente (DNI, Carné de extranjería, etc.) el DNI deberá contar con la constancia de votación al día, dispensa ó pago de multas.', 8),
(35, 'Si la legalización de firmas es con poder o en representación de una entidad, se requiere la vigencia de poder expedida por los Registros Públicos (la vigencia de poder no debe ser mayor a 30 días de antigüedad).', 8),
(36, 'Solicitud debidamente firmada o minuta suscrita por las partes y autorizada por abogado colegiado.', 9),
(37, 'Proporcionar generales de ley del solicitante o de los contratantes.', 9),
(38, 'Adjuntar fotocopia de documentos de identidad vigentes (DNI, CIP, Carnet de Extranjería, Etc.) del solicitante o contratantes.', 9),
(39, 'Cancelar importe de las publicaciones, cuando corresponda.', 9),
(40, 'Adjuntar copias originales de las partidas del Registro Civil que correspondan.', 9),
(41, 'Solicitud debidamente firmada o minuta suscrita por las partes y autorizada por abogado colegiado.', 10),
(42, 'Proporcionar generales de ley del solicitante o de los contratantes', 10),
(43, 'Adjuntar fotocopia de documentos de identidad vigentes (DNI, CIP, Carnet de Extranjería, Etc.) del solicitante o contratantes.', 10),
(44, 'Cancelar importe de las publicaciones, cuando corresponda.', 10),
(45, 'Adjuntar copias originales de las partidas del Registro Civil que correspondan.', 10),
(46, 'Copia simples y legibles de los documentos de identidad de los solicitantes.', 11),
(47, 'Copia certificada del acta o partida de matrimonio de los peticionantes expedida con no más de 3 meses de antigüedad respecto de la fecha de presentación de la solicitud.', 11),
(48, 'Declaración jurada, con firma y huella digital de los peticionantes de no tener hijos menores de edad o mayores con incapacidad.', 11),
(49, 'De ser el caso, copia certificada del acta de nacimiento del hijo menor de edad o incapaz expedida con no más de tres meses respecto de la fecha de presentación.', 11),
(50, 'De ser el caso, copia certificada de la sentencia judicial firme o acta de conciliación que establezca los regímenes de patria potestad, tenencia, alimentos y visitas, o de curatela, alimentos y vistas en caso de tener hijo menor de edad o mayor de edad con discapacidad.', 11),
(51, 'Testimonio de la escritura pública de separación de patrimonios o de sustitución o liquidación de régimen patrimonial inscrita en los Registros Públicos.', 11),
(52, 'Declaración jurada con firma y huella de los solicitantes de carecer de bienes sujetos al régimen de sociedad de gananciales.', 11),
(53, 'Declaración jurada del último domicilio conyugal, suscrita por ambos y con la huella digital de ambos solicitantes.', 11),
(54, 'Partida Errada.', 12),
(55, 'Partida de sustento.', 12),
(56, 'Copia del documento de identidad del solicitante.', 12),
(57, 'Presentar el título valor a más tardar al octavo día de la fecha de vencimiento del mismo.', 13),
(58, 'Presentar el título valor completamente firmado, sin enmendaduras, raspones ni borrones.', 13),
(59, 'Poder Amplio y General', 14),
(60, 'Poder para pleitos', 14),
(61, 'Poder para divorcio', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_servicios_notariales`
--

CREATE TABLE `h_servicios_notariales` (
  `id` int(11) NOT NULL,
  `vista` varchar(300) DEFAULT NULL,
  `subvista` varchar(500) NOT NULL,
  `lugar` longtext,
  `nota` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_servicios_notariales`
--

INSERT INTO `h_servicios_notariales` (`id`, `vista`, `subvista`, `lugar`, `nota`) VALUES
(1, 'Escrituras Públicas', 'Compra Venta', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(2, 'Escrituras Públicas', 'Poderes', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(3, 'Transferencias Vehiculares', '', '', 'Deberán estar presentes comprador(es) y vendedor(es) conjuntamente'),
(4, 'Asuntos no Contenciosos', 'Sucesión Interesada', '', 'La solicitud deberá ser presentada por cualquiera de los interesados a que alude el artículo 815 del Código Civil, o por el integrante sobreviviente de la unión de hecho reconocida conforme a Ley.'),
(5, 'Asuntos no Contenciosos', 'Patrimonio Familiar', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(6, 'Cartas Notariales', '', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(7, 'Legalizaciones', 'De copias', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(8, 'Legalizaciones', 'De firmas', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(9, 'Escrituras Públicas', 'Constitucion de Sociedades', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(10, 'Escrituras Públicas', 'Constitucion de Hipotecas', '', 'Cualquier información adicional, se le dara a conocer de manera personal en nuestra oficina.'),
(11, 'Asuntos no Contenciosos', 'Divorcios', '', 'Pueden solicitar este trámite las personas que hayan contraído matrimonio con antigüedad no menor de 2 años. El matrimonio debe haber sido celebrado en Puerto Maldonado o tener como último domicilio conyugal uno ubicado en Puerto Maldonado.'),
(12, 'Asuntos no Contenciosos', 'Rectificación de Partidas', '', 'Las rectificaciones que tengan por objeto corregir los errores y omisiones de nombre, apellidos, fecha de nacimiento, de matrimonio, defunción u otros que resulten evidentes del tenor de la propia partida o de otros documentos probatorios, se tramitarán ante un notario.'),
(13, 'Protestos', '', '', 'Tomar en cuenta que la dirección de notificación debe estar dentro de la provincia de Tambopata.'),
(14, 'Poderes', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_slide`
--

CREATE TABLE `h_slide` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  `slide` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_slide`
--

INSERT INTO `h_slide` (`id`, `titulo`, `url`, `slide`) VALUES
(1, 'Certificaciones', 'http://localhost:4000/#/Client/Servicios_notariales/Certificaciones', 'slide0001.jpg'),
(2, 'Escrituras Públicas', 'http://localhost:4000/#/Client/Servicios_notariales/Estructuras_Publicas/Compra_Venta', 'slide0002.jpg'),
(3, 'Cartas Notariales', 'http://localhost:4000/#/Client/Servicios_notariales/Cartas_Notariales', 'slide0003.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h_valores`
--

CREATE TABLE `h_valores` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `descripcion` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h_valores`
--

INSERT INTO `h_valores` (`id`, `titulo`, `descripcion`) VALUES
(3, 'Colaboración', 'Todas las acciones y decisiones que se lleven a cabo en la notaría estarán presididas por un espíritu de colaboración entre directivos y empleados independientes de su nivel jerárquico ( Relaciones Interpersonales).'),
(4, 'Autonomía', 'Se favorecerá, a todos los niveles, el trabajo con autonomía y la asunción de responsabilidades individuales.'),
(5, 'Innovación', 'Se apoyarán todas las ideas y sugerencias, individuales o de equipo, que propongan mejoras métodos de trabajo, servicios, relaciones humanas o cualquier otro aspecto de la notaría.'),
(6, 'Desarrollo del Personal', 'Se mantendrá una política de formación continua con el objetivo de favorecer el desarrollo constante de los empleados, tanto desde el punto de visto profesional, como desde el personal.'),
(7, 'Seguridad Jurídica', 'Se cuidará que la actuación de la Notaría este enmarcada siempre en la Ley y brinde seguridad jurídica a los usuarios que soliciten nuestro servicios.'),
(8, 'Servicio al cliente', 'Las acciones y decisiones de todo el personal estarán dirigidas a satisfacer las necesidades de los clientes.');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `h_admin`
--
ALTER TABLE `h_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_enlace`
--
ALTER TABLE `h_enlace`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_informe`
--
ALTER TABLE `h_informe`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_nosotros`
--
ALTER TABLE `h_nosotros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_noticia`
--
ALTER TABLE `h_noticia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_preguntas`
--
ALTER TABLE `h_preguntas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_presentacion`
--
ALTER TABLE `h_presentacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_requisito`
--
ALTER TABLE `h_requisito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_servicios_notariales`
--
ALTER TABLE `h_servicios_notariales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_slide`
--
ALTER TABLE `h_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `h_valores`
--
ALTER TABLE `h_valores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `h_admin`
--
ALTER TABLE `h_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `h_enlace`
--
ALTER TABLE `h_enlace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `h_informe`
--
ALTER TABLE `h_informe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `h_nosotros`
--
ALTER TABLE `h_nosotros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `h_noticia`
--
ALTER TABLE `h_noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `h_preguntas`
--
ALTER TABLE `h_preguntas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `h_presentacion`
--
ALTER TABLE `h_presentacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `h_requisito`
--
ALTER TABLE `h_requisito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `h_servicios_notariales`
--
ALTER TABLE `h_servicios_notariales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `h_slide`
--
ALTER TABLE `h_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `h_valores`
--
ALTER TABLE `h_valores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
